<?php
include_once('includes/basepath.php');
if(!isset($_SESSION['username'])){
	header('Location: ./index.php');
	exit;
}
if(isset($_REQUEST["id"])) $receipt_id = $_REQUEST["id"]; else $receipt_id = 0;
if(!isset($_REQUEST["lastrec"])){
	$sSQL = "SELECT * FROM receipt_master WHERE receipt_id = ".$receipt_id;
	$rs = mysql_query($sSQL) or print(mysql_error());
	$row = mysql_fetch_array($rs);

}else{
	$sSQL = "SELECT * FROM receipt_master WHERE company_id = ".$company_id." AND receipt_time < '".date("Y-m-d H:i:s")."' ORDER BY receipt_id DESC LIMIT 0,1";
	$rs = mysql_query($sSQL) or print(mysql_error());
	$row = mysql_fetch_array($rs);
	$receipt_id = $row["receipt_id"];
}

//get company details
$cid = $row['company_id'];
$qry = "SELECT * FROM company WHERE company_id = '$cid'";
$resu= mysql_query($qry) or print(mysql_error());
$rescomp = mysql_fetch_array($resu);

//get draw details
$drawid = $row['draw_id'];
$drqry = "SELECT * FROM draw WHERE draw_id = '$drawid'";
$drres= mysql_query($drqry) or print(mysql_error());
$resdraw = mysql_fetch_array($drres);

$row['company_id'];
$sSQL = "SELECT * FROM receipt_details WHERE receipt_id = ".$receipt_id;
$rs1 = mysql_query($sSQL) or print(mysql_error());

?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="js/cycle.js"></script>
	<script type="text/javascript" src="js/cycle.tile.js"></script>	
</head>
<body>
	<?php
		
	?>
	<div class="mainwrapper" style="border:1px solid black;width:300px;margin:0px auto;background:white;padding:10px;">		
		<div style="font-size:20px; text-align:center; border-bottom:dotted 2px #514F4F; padding-bottom:5px; margin-bottom:5px;">Acknowledgement Receipt - <?php echo $receipt_id?></div>
		<?php if($row["hash_key"] != "" && file_exists("images/codes/".$row["hash_key"].".gif")){ ?>
		<div style="text-align:center"><img src="<?php echo "images/codes/".$row["hash_key"].".gif"; ?>" /></div>
		<?php } ?>
		<div><span><?php echo date("d/m/Y h:i:s A",strtotime($row['receipt_time']));?></span>&nbsp;&nbsp;&nbsp;<span>C.ID - <?php echo $row['retailer_id'];?></span></div>
		<div><b><?php echo $rescomp['company_name']?></b></div>
		<div><b><span>Date : <?php echo date("d/m/Y",strtotime($resdraw['drawdatetime']));?></span>&nbsp;&nbsp;&nbsp;<span>Time : <?php echo date("h:i A",strtotime($resdraw['drawdatetime']));?></span></b></div>
		<?php 
		$totqty = $totamt = 0;
		if(mysql_num_rows($rs1) > 0){		
			while($rows = mysql_fetch_array($rs1)){
		
					echo '<div>'.$yantra[$rows["product_id"]-1].' * '.$rows["quantity"].'</div>';
					//echo "<pre>";print_r($rows);
					$totqty += $rows["quantity"];
					$totamt = $totamt + ($rows["quantity"]*$rows["product_price"]);
			}
		}
		
		?>
		<br><br>
		<div><b><span>Product Qty : <?php echo $totqty;?></span>&nbsp;&nbsp;&nbsp;<span>Total Amt : <?php echo formatAmt($totamt);?></span></b></div>
		<div>*Conditions Apply :</div>
		<div>For Terms & Conditions P.T.O.</div>
		<div>This Acknowlegement slip should be</div>
		<div>retained to redeem the products.</div>
	</div>
	<script type="text/javascript">javascript:window.print();</script>
</body>
</html>