-- Database: `gujaratgold` --
-- Table `company` --
CREATE TABLE `company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `draw_amount` double DEFAULT '0',
  `jackput_amount` double DEFAULT '0',
  `draw_amount2` double DEFAULT '0',
  `jackput_amount2` double DEFAULT '0',
  `jp2_amount` double DEFAULT NULL,
  `jp3_amount` double DEFAULT NULL,
  `jp4_amount` double DEFAULT NULL,
  `jp5_amount` double DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `company` (`company_id`, `company_name`, `product_price`, `draw_amount`, `jackput_amount`, `draw_amount2`, `jackput_amount2`, `jp2_amount`, `jp3_amount`, `jp4_amount`, `jp5_amount`) VALUES
(1, 'GUJARAT GOLD VAPI', '10', '100', '200', '0', '0', '', '', '', ''),
(2, 'NAV RATANAM', '10', '100', '200', '0', '0', '', '', '', ''),
(3, 'Gujarat Gold Lucky Draw', '10', '100', '200', '300', '400', '200', '300', '400', '500');

-- Table `data_sheet` --
CREATE TABLE `data_sheet` (
  `data_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `ds_name` varchar(222) NOT NULL,
  `ds_all_trades` double NOT NULL,
  `ds_long_trades` double NOT NULL,
  `ds_short_trades` double NOT NULL,
  `do_date` datetime NOT NULL,
  `do_buy_sell` varchar(222) NOT NULL,
  `do_price` double NOT NULL,
  `do_qty` double NOT NULL,
  `do_current_value` double NOT NULL,
  `dt_brokerage_fees` varchar(222) NOT NULL,
  `dt_entry_date` varchar(222) NOT NULL,
  `dt_exit_date` varchar(222) NOT NULL,
  `dt_type` varchar(222) NOT NULL,
  `dt_no_bars` varchar(222) NOT NULL,
  `dt_absolute_performance` double NOT NULL,
  `dt_brokerage` double NOT NULL,
  `hl_file_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Table `draw` --
CREATE TABLE `draw` (
  `draw_id` int(11) NOT NULL AUTO_INCREMENT,
  `drawdatetime` datetime DEFAULT NULL,
  `win_product_id` int(11) DEFAULT NULL,
  `win_amount` double NOT NULL DEFAULT '0',
  `is_jackpot` enum('NO','YES') NOT NULL,
  `win_product_id2` int(11) DEFAULT NULL,
  `win_amount2` double NOT NULL DEFAULT '0',
  `is_jackpot2` enum('NO','YES') NOT NULL,
  `percent` double NOT NULL DEFAULT '0',
  `last_update_time` datetime DEFAULT NULL,
  `last_update_ip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`draw_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

INSERT INTO `draw` (`draw_id`, `drawdatetime`, `win_product_id`, `win_amount`, `is_jackpot`, `win_product_id2`, `win_amount2`, `is_jackpot2`, `percent`, `last_update_time`, `last_update_ip`) VALUES
(1, '2017-01-16 08:00:00', 1, '100', 'NO', , '0', 'NO', '0', '', ''),
(2, '2017-01-16 08:15:00', 6, '100', 'YES', , '0', 'NO', '0', '', ''),
(3, '2017-01-16 08:30:00', 2, '100', 'NO', , '0', 'NO', '0', '', ''),
(4, '2017-01-16 08:45:00', 7, '100', 'NO', , '0', 'NO', '0', '', ''),
(5, '2017-01-16 09:00:00', 8, '200', 'YES', , '0', 'NO', '0', '', ''),
(6, '2017-01-16 09:15:00', 6, '100', 'NO', , '0', 'NO', '0', '', ''),
(7, '2017-01-16 09:30:00', 8, '100', 'NO', , '0', 'NO', '0', '', ''),
(8, '2017-01-16 09:45:00', 8, '100', 'NO', , '0', 'NO', '0', '', ''),
(9, '2017-01-16 10:00:00', 5, '100', 'NO', , '0', 'NO', '0', '', ''),
(10, '2017-01-16 10:15:00', 3, '100', 'NO', , '0', 'NO', '0', '', ''),
(11, '2017-01-16 10:30:00', 10, '100', 'NO', , '0', 'NO', '0', '', ''),
(12, '2017-01-16 10:45:00', 10, '100', 'NO', , '0', 'NO', '0', '', ''),
(13, '2017-01-16 11:00:00', 9, '100', 'NO', , '0', 'NO', '0', '', ''),
(14, '2017-01-16 11:15:00', 1, '100', 'NO', , '0', 'NO', '0', '', ''),
(15, '2017-01-16 11:30:00', 8, '100', 'NO', , '0', 'NO', '0', '', ''),
(16, '2017-01-16 11:45:00', 1, '100', 'NO', , '0', 'NO', '0', '', ''),
(17, '2017-01-16 12:00:00', 2, '100', 'NO', , '0', 'NO', '0', '', ''),
(18, '2017-01-16 12:15:00', 8, '100', 'NO', , '0', 'NO', '0', '', ''),
(19, '2017-01-16 12:30:00', 7, '100', 'NO', , '0', 'NO', '0', '', ''),
(20, '2017-01-16 12:45:00', 1, '100', 'NO', , '0', 'NO', '0', '', ''),
(21, '2017-01-16 13:00:00', 3, '100', 'NO', , '0', 'NO', '0', '', ''),
(22, '2017-01-16 13:15:00', 3, '100', 'NO', , '0', 'NO', '0', '', ''),
(23, '2017-01-16 13:30:00', 4, '100', 'NO', , '0', 'NO', '0', '', ''),
(24, '2017-01-16 13:45:00', 9, '100', 'NO', , '0', 'NO', '0', '', ''),
(25, '2017-01-16 14:00:00', 6, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(26, '2017-01-16 14:15:00', 1, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(27, '2017-01-16 14:30:00', 8, '200', 'YES', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(28, '2017-01-16 14:45:00', 2, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(29, '2017-01-16 15:00:00', 3, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(30, '2017-01-16 15:15:00', 7, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(31, '2017-01-16 15:30:00', 1, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(32, '2017-01-16 15:45:00', 7, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(33, '2017-01-16 16:00:00', 1, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(34, '2017-01-16 16:15:00', 8, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(35, '2017-01-16 16:30:00', 10, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(36, '2017-01-16 16:45:00', 9, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(37, '2017-01-16 17:00:00', 3, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(38, '2017-01-16 17:15:00', 8, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(39, '2017-01-16 17:30:00', 10, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(40, '2017-01-16 17:45:00', 5, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1'),
(41, '2017-01-16 18:00:00', 5, '100', 'NO', , '0', 'NO', '0', '2017-01-16 13:55:59', '127.0.0.1');

-- Table `product` --
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO `product` (`product_id`, `company_id`, `product_name`) VALUES
(1, 2, 'One'),
(2, 2, 'Two'),
(3, 2, 'Three'),
(4, 2, 'Four'),
(5, 2, 'Five'),
(6, 2, 'Six'),
(7, 2, 'Seven'),
(8, 2, 'Eight'),
(9, 2, 'Nine'),
(10, 2, 'Ten');

-- Table `receipt_details` --
CREATE TABLE `receipt_details` (
  `receipt_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`receipt_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1126 DEFAULT CHARSET=latin1;

INSERT INTO `receipt_details` (`receipt_details_id`, `receipt_id`, `product_id`, `quantity`, `product_price`) VALUES
(1116, 251, 1, 1, '10'),
(1117, 251, 2, 1, '10'),
(1118, 251, 3, 1, '10'),
(1119, 251, 4, 1, '10'),
(1120, 251, 5, 1, '10'),
(1121, 251, 6, 1, '10'),
(1122, 251, 7, 1, '10'),
(1123, 251, 8, 1, '10'),
(1124, 251, 9, 1, '10'),
(1125, 251, 10, 1, '10');

-- Table `receipt_master` --
CREATE TABLE `receipt_master` (
  `receipt_id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_time` datetime NOT NULL,
  `receipt_ip` varchar(15) DEFAULT NULL,
  `retailer_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `draw_id` int(11) NOT NULL,
  `hash_key` varchar(13) DEFAULT NULL,
  `receipt_cancel` tinyint(4) NOT NULL DEFAULT '0',
  `cancel_time` datetime DEFAULT NULL,
  `cancel_ip` varchar(15) DEFAULT NULL,
  `receipt_scan` tinyint(4) NOT NULL DEFAULT '0',
  `user_commission` double NOT NULL DEFAULT '0',
  `win_time` datetime DEFAULT NULL,
  `win_ip` varchar(15) DEFAULT NULL,
  `scan_time` datetime DEFAULT NULL,
  PRIMARY KEY (`receipt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=latin1;

INSERT INTO `receipt_master` (`receipt_id`, `receipt_time`, `receipt_ip`, `retailer_id`, `company_id`, `draw_id`, `hash_key`, `receipt_cancel`, `cancel_time`, `cancel_ip`, `receipt_scan`, `user_commission`, `win_time`, `win_ip`, `scan_time`) VALUES
(251, '2017-01-16 15:18:38', '127.0.0.1', 10032, 3, 31, '58497N017C834', '0', '', '', '1', '0', '', '', '2017-01-16 15:30:22');

-- Table `trans_for` --
CREATE TABLE `trans_for` (
  `trans_for_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_for` varchar(255) NOT NULL,
  PRIMARY KEY (`trans_for_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Table `transaction` --
CREATE TABLE `transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `retailer_id` int(11) DEFAULT NULL,
  `cr_dr` varchar(255) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `trans_for_id` int(11) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `transaction_time` datetime DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

INSERT INTO `transaction` (`transaction_id`, `retailer_id`, `cr_dr`, `amount`, `receipt_id`, `trans_for_id`, `notes`, `transaction_time`) VALUES
(24, 10033, 'Credit', '30000', , , 'tertr', '2017-01-16 11:15:25'),
(25, 10033, 'Credit', '1000', , , 'teststt', '2017-01-16 11:23:07'),
(26, 10038, 'Credit', '26000', , , 'Opening', '2017-01-16 11:29:29'),
(27, 10033, 'Credit', '1000', , , 'test', '2017-01-16 12:28:04'),
(28, 10039, 'Credit', '1000', , , 'Opening', '2017-01-16 12:29:56'),
(29, 10040, 'Credit', '2000', , , 'Opening', '2017-01-16 12:30:33'),
(30, 10033, 'Credit', '850', , , 'rtwertwere', '2017-01-16 12:31:31'),
(31, 10033, 'Debit', '850', , , '', '2017-01-16 12:33:30'),
(32, 10033, 'Credit', '850', , , '', '2017-01-16 14:09:07'),
(33, 10033, 'Credit', '3000', , , '', '2017-01-16 14:41:10'),
(34, 10037, 'Debit', '10000', , , '', '2017-01-16 15:57:54');

-- Table `users` --
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `current_balance` double NOT NULL,
  `usertype` int(11) NOT NULL COMMENT '0=admin,1=retailer, 2=agent',
  `user_commission` double NOT NULL DEFAULT '0',
  `user_ip` varchar(15) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `online` datetime DEFAULT NULL,
  `city` varchar(15) DEFAULT NULL,
  `note` text,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10041 DEFAULT CHARSET=latin1;

INSERT INTO `users` (`user_id`, `parent_id`, `username`, `password`, `current_balance`, `usertype`, `user_commission`, `user_ip`, `is_active`, `online`, `city`, `note`) VALUES
(1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '47581', 0, '0', '', 1, '2017-01-15 23:13:26', '', ''),
(30, 1, 'omtest', '30dd1ab18633e1f914b95db672cb6450', '48849.5', 0, '5', '', 1, '2017-01-16 18:04:48', '', ''),
(10031, 1, 'gold6', '4a4314ef967aad20a9e7c423bc16e39c', '5000', 1, '0', '', 1, '', '', ''),
(10030, 1, 'gold5', 'b53b3a3d6ab90ce0268229151c9bde11', '5000', 1, '0', '', 1, '', '', ''),
(10029, 1, 'gold4', 'ae11976937537e4c1206237dea035331', '5000', 1, '0', '', 1, '', '', ''),
(10028, 1, 'gold3', '745e9cdc76ffef9a8c1ae15e3f2e8cf9', '5000', 1, '0', '', 1, '2017-01-13 09:06:46', '', ''),
(10027, 1, 'gold2', '2d387ab98437e5f528a87b031a086256', '4600', 1, '0', '', 1, '2017-01-15 09:46:37', '', ''),
(10026, 1, 'gold1', '0120a4f9196a5f9eb9f523f31f914da7', '4450', 1, '0', '', 1, '2017-01-15 17:15:41', '', ''),
(10025, 1, 'jayesh', 'e10adc3949ba59abbe56e057f20f883e', '5374.850000000001', 1, '5', '', 1, '2017-01-04 11:32:27', 'Rajkot', ''),
(10024, 1, 'rameshbhai', 'e10adc3949ba59abbe56e057f20f883e', '2736.5000000000027', 1, '10', '', 1, '2017-01-08 16:12:27', 'rajkot - Gujrat', ''),
(10032, 1, 'agent1', '486dac28e554a94e65feb6bf5acd98bc', '20000', 2, '0', '', 1, '2017-01-16 15:49:45', '', ''),
(10033, 10032, 'a1gold1', '98ea8da977be9e2b9fa25bce709b4799', '40850', 1, '0', '', 1, '2017-01-16 17:59:16', '', ''),
(10034, 10032, 'a1gold2', '66738d96b4715a8615a1109f27068416', '5000', 2, '0', '', 1, '', '', ''),
(10035, 10032, 'a1gold3', 'e1223695638fcca42425f1bfb843e489', '5003', 1, '0', '', 1, '', '', ''),
(10036, 1, 'Jigo', 'cb57cdb7cc459dc6fbbc33f91485b5e2', '5000', 2, '0', '', 1, '2017-01-16 16:57:46', '', ''),
(10037, 10036, 'Kekado', 'f6c0c55d8d0a5cd1f69ceaf3d5b4e108', '0', 1, '0', '', 1, '2017-01-15 20:57:17', '', ''),
(10038, 10032, 't1t1t1', '01357dec5d822bc70aebb34102df1e78', '26000', 1, '0', '', 1, '', 't1t1t1', 't1t1t1'),
(10039, 10032, 'qqq', 'b2ca678b4c936f905fb82f2733f5297f', '1000', 1, '0', '', 1, '', 'qqq', 'qqq'),
(10040, 10032, 'zzz', 'f3abb86bd34cf4d52698f14c0da1dc60', '2000', 1, '0', '', 1, '', 'zzz', 'zzz');

-- Table `users_login` --
CREATE TABLE `users_login` (
  `users_login_id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `login_time` datetime NOT NULL,
  `user_ip` varchar(15) DEFAULT NULL,
  `valid_invalid` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `users_login` (`users_login_id`, `username`, `login_time`, `user_ip`, `valid_invalid`) VALUES
(0, 'omtest', '2017-01-16 09:37:04', '127.0.0.1', 'Invalid'),
(0, 'omtest', '2017-01-16 09:37:28', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 09:46:52', '127.0.0.1', 'Valid'),
(0, 'omtest', '2017-01-16 09:54:35', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 10:02:28', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 10:02:45', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 10:05:19', '127.0.0.1', 'Valid'),
(0, 'omtest', '2017-01-16 10:50:44', '127.0.0.1', 'Valid'),
(0, 'a1gold1', '2017-01-16 11:22:56', '127.0.0.1', 'Valid'),
(0, 'a1gold1', '2017-01-16 11:24:13', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 11:24:32', '127.0.0.1', 'Invalid'),
(0, 'agent1', '2017-01-16 11:24:38', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 12:27:25', '127.0.0.1', 'Invalid'),
(0, 'agent1', '2017-01-16 12:27:34', '127.0.0.1', 'Valid'),
(0, 'omtest', '2017-01-16 12:30:49', '127.0.0.1', 'Valid'),
(0, 'a1gold1', '2017-01-16 12:32:28', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 12:32:46', '127.0.0.1', 'Valid'),
(0, 'omtest', '2017-01-16 12:50:21', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 01:09:32', '127.0.0.1', 'Invalid'),
(0, 'agent1', '2017-01-16 01:09:42', '127.0.0.1', 'Valid'),
(0, 'omtest', '2017-01-16 01:52:08', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 02:08:41', '127.0.0.1', 'Valid'),
(0, 'omtest', '2017-01-16 02:39:16', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 02:39:44', '127.0.0.1', 'Valid'),
(0, 'omtest', '2017-01-16 02:41:20', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 02:51:29', '127.0.0.1', 'Valid'),
(0, 'omtest', '2017-01-16 02:51:45', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 02:52:03', '127.0.0.1', 'Valid'),
(0, 'omtest', '2017-01-16 03:01:52', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 03:18:00', '127.0.0.1', 'Valid'),
(0, 'omtest', '2017-01-16 03:30:49', '127.0.0.1', 'Valid'),
(0, 'a1gold1', '2017-01-16 03:45:33', '127.0.0.1', 'Valid'),
(0, 'agent1', '2017-01-16 03:49:43', '127.0.0.1', 'Valid'),
(0, 'jigo', '2017-01-16 03:56:05', '127.0.0.1', 'Invalid'),
(0, 'jigo', '2017-01-16 03:56:11', '127.0.0.1', 'Valid'),
(0, 'a1gold1', '2017-01-16 05:25:16', '127.0.0.1', 'Valid'),
(0, 'a1gold1', '2017-01-16 05:59:15', '127.0.0.1', 'Valid'),
(0, 'omtest', '2017-01-16 05:59:26', '127.0.0.1', 'Valid');

