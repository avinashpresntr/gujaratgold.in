$(function() {
	$("#partyName").focus();
	$( "#color-entry" ).submit(function() {
  	if($("#partyName").val() == ""){
			alert("Enter Party.");
			$("#partyName").focus();
			return false;
		}
		if($("#itemId").val() == "0"){
			alert("Select Item.");
			$("#itemId").focus();
			return false;
		}
		/*if($("#rate").val() == ""){
			alert("Enter Rate.");
			$("#rate").focus();
			return false;
		}*/
		if($("#qty").val() == ""){
			alert("Enter Qty");
			$("#qty").focus();
			return false;
		}
		$("#color-entry :input").attr("disabled", false);
		return true;
	});
	
	$("[data-mask]").inputmask();
	
	$('#tradeList').dataTable();
	
	$(".onlynum").keydown(function (e) {
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
          (e.keyCode == 65 && e.ctrlKey === true) || 
          (e.keyCode >= 35 && e.keyCode <= 39)) {
          return;
      }
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
  });
  
  $('#rate, #qty, #vat').blur(function() {
		calRow();
	});
	
	$('#itemId').change(function() {
		calRow();
	});
		
});

function deltrade(id){
	var r = confirm("Confirm to delete this record?");
	if (r == true) {
		window.location.assign("trade_delete.php?tradeId="+id);
	}
}

function calRow(){
	multiply = $('#itemId').find(':selected').attr('datavalue');
	rate = $('#rate').val();
	qty = $('#qty').val();
	if(rate > 0 && qty > 0){
		vatAmount = 1;
		if($('#vat').val() > 0)
			vatAmount = $('#vat').val();
    else
      vatAmount = 0;
		tmpAmount = rate*qty + rate*qty*vatAmount/100;
		if(multiply > 0) 
      tmpAmount = tmpAmount * multiply;
		$('#amount').val(tmpAmount);
	}else{
		$('#amount').val('');
	}
}