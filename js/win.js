var LeftSideTime;
var RightSideTime;

var diffTime   = "";
var currenTime = "";
var flag     = true;
var ajaxFlag = true;
var montharray;
var serverdate;
if (typeof is_mobile === 'undefined') {
var is_mobile = false;
    // the variable is defined
}

function customAlert($msg)
{
    swal({title: $msg,confirmButtonColor: "#F660AB"});
}

function padlength(what){
  var output=(what.toString().length==1)? "0"+what : what
  return output
}

// Execute when click on Current Button
function getCurrentData(isupcomming){
  console.log("Calling.... -> getCurrentData ");
  clearInterval(LeftSideTime);
  clearInterval(RightSideTime);
  ajaxFlag = true;
  flag = true;

  if(isupcomming == 0)
    qStr = "act=loadData";
  else
    qStr = "act=loadData&drawid="+isupcomming;
  console.log("ajax call @ line# 90 [LoadData]");
  $.ajax({
    url:"ajax.php",
    data:qStr,
    type:"POST",
    datatype:"json",
    success:function(data){
      console.log("ajax responce of line# 90 [LoadData]");
      console.log(data);
    //customAlert("====="+data);
    //return false;
    if(data){
      data = JSON.parse(data);
      //customAlert(data[3]);
      if(data[3] != ""){
        $("#draw_id").val(data[3]);
        $("#currentFullDateTime").html(data[2]);
        $("#ctime").html(data[1]);
        //customAlert(data[1]);
        //clearInterval(LeftSideTime);
        //LeftSideTime = setInterval(startLeftTime,1000);

        diffTime     = data[0];
        currenTime   = data[2];
        montharray   = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
        serverdate   = new Date(currenTime);
        update();
        //$('#nextDrawTime').text(data[1]);
        
      }
    }else{
      $("#ctime").html('');
      $("#ltime").html('');
      $("#currentFullDateTime").html('');
      clearInterval(LeftSideTime);
      clearInterval(RightSideTime);
    }
    },
    error:function(){
      
      //customAlert("Error while loading try again");
    }
  });
}

function displaytime(){
  serverdate.setSeconds(serverdate.getSeconds()+1)
  var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear()
  var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds())
  document.getElementById("currentTimer").innerHTML=timestring
}


function update() {
  $('#ltime').text(secondsToTime(diffTime));
  diffTime    = diffTime - 1;
  currenTime  = parseInt(currenTime) + 1;
  if(diffTime <= 0 && ajaxFlag){
    window.location.reload();
    return false;
  }
  if(diffTime % 60 == 0){
    window.location.reload();
    return false;
  }
  if(flag){
    flag = false;
    LeftSideTime = setInterval(function(){ update(); }, 1000);
    RightSideTime = setInterval("displaytime()", 1000)
  }
}

function secondsToTime(seconds){
  if(seconds<0)
  seconds=0;
  var sec_num = parseInt(seconds);
  var hours   = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);
  if (hours   < 10) {hours   = "0"+hours;}
  if (minutes < 10) {minutes = "0"+minutes;}
  if (seconds < 10) {seconds = "0"+seconds;}
  var time    = hours+':'+minutes+':'+seconds;
  return time; 
}

function secondsToTime(seconds){
  if(seconds<0)
  seconds=0;
  var sec_num = parseInt(seconds);
  var hours   = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);
  if (hours   < 10) {hours   = "0"+hours;}
  if (minutes < 10) {minutes = "0"+minutes;}
  if (seconds < 10) {seconds = "0"+seconds;}
  var time    = hours+':'+minutes+':'+seconds;
  return time; 
}

function msToTime(duration) {
  var milliseconds = parseInt((duration%1000)/100)
      , seconds = parseInt((duration/1000)%60)
      , minutes = parseInt((duration/(1000*60))%60)
      , hours = parseInt((duration/(1000*60*60))%24);
  
  hours = (hours < 10) ? "0" + hours : hours;
  minutes = (minutes < 10) ? "0" + minutes : minutes;
  seconds = (seconds < 10) ? "0" + seconds : seconds;
  
  return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
}


$(document).ready(function() {
  getCurrentData(0);
});
