	var diffTime   = "";
		var currenTime = "";
		var flag     = true;
		var ajaxFlag = true;
		var montharray;
		var serverdate;
		$(document).ready(function() { 
			ajax();
		});
	
		function ajax(){
		    $.ajax({
		        type: "POST",
		        url: "loadtime.php",
		        success: function(data){
					dataSplitted =  JSON.parse(data);
		    //    	dataSplitted = data.split(",");
					diffTime     = dataSplitted[0];
					currenTime   = dataSplitted[2]; 
					montharray   = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
					serverdate   = new Date(currenTime);
					ajaxFlag = true; 
					update();
					$('#ctime').text(dataSplitted[1]);
				}
		    });
		}
	
		function padlength(what){
			var output=(what.toString().length==1)? "0"+what : what
			return output
		}
	
		function displaytime(){
			serverdate.setSeconds(serverdate.getSeconds()+1)
			var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear()
			var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds())
			document.getElementById("currentTimer").innerHTML = conv24to12clock(timestring);
		}
		
		function update() {
			$('#ltime').text(secondsToTime(diffTime));
			diffTime    = diffTime - 1;
			currenTime  = parseInt(currenTime) + 1;
			if(diffTime==0 && ajaxFlag){
				ajaxFlag =  false;
				ajax();
				getResult();
			} 
			if(flag){
				flag = false;
				setInterval(function(){update(); }, 1000);
				setInterval("displaytime()", 1000)
			}
		}
		
		function secondsToTime(seconds){
			 var sec_num = parseInt(seconds);
		     var hours   = Math.floor(sec_num / 3600);
		     var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
		     var seconds = sec_num - (hours * 3600) - (minutes * 60);        
		     if (hours   < 10) {hours   = "0"+hours;}
		     if (minutes < 10) {minutes = "0"+minutes;}
		     if (seconds < 10) {seconds = "0"+seconds;}
		     var time    = hours+':'+minutes+':'+seconds;
		     return time; 
	 	}
		function conv24to12clock(time) {
			 time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

			  if (time.length > 1) { // If time format correct
			    time = time.slice (1);  // Remove full string match value
			    time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
			    time[0] = +time[0] % 12 || 12; // Adjust hours
			  }
			  return time.join (''); // return adjusted time or original string
		}
	 