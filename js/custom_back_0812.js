$(function () {
	$('#receipt').on('keypress', function (e) {
	  if (e.which == 13) {
	    barcode = 0;
	    if($('#1').val() > 9999) barcode = $('#1').val();
	    if($('#2').val() > 9999) barcode = $('#2').val();
	    if($('#3').val() > 9999) barcode = $('#3').val();
	    if($('#4').val() > 9999) barcode = $('#4').val();
	    if($('#5').val() > 9999) barcode = $('#5').val();
	    if($('#6').val() > 9999) barcode = $('#6').val();
	    if($('#7').val() > 9999) barcode = $('#7').val();
	    if($('#8').val() > 9999) barcode = $('#8').val();
	    if($('#9').val() > 9999) barcode = $('#9').val();
	    if($('#10').val() > 9999) barcode = $('#10').val();
	    if(barcode > 9999){
	    	clearElements();
	    	$('#scancode').val(barcode);
	    	$('#loadWinner').submit();
	    }
	    return false;
	  }
	});
  
  $('#receipt').on('submit', function (e) {
    e.preventDefault();
    barcode = 0;
    if($('#1').val() > 9999) barcode = $('#1').val();
    if($('#2').val() > 9999) barcode = $('#2').val();
    if($('#3').val() > 9999) barcode = $('#3').val();
    if($('#4').val() > 9999) barcode = $('#4').val();
    if($('#5').val() > 9999) barcode = $('#5').val();
    if($('#6').val() > 9999) barcode = $('#6').val();
    if($('#7').val() > 9999) barcode = $('#7').val();
    if($('#8').val() > 9999) barcode = $('#8').val();
    if($('#9').val() > 9999) barcode = $('#9').val();
    if($('#10').val() > 9999) barcode = $('#10').val();

    if(barcode > 9999){
    	clearElements();
    	$('#scancode').val(barcode);
    	$('#loadWinner').submit();
    }else{
    	if(parseFloat($('.currentBalance').html()) > 0){
    	if($('#draw_id').val() > 0){
    	if($('#1').val() > 0 || $('#2').val() > 0 || $('#3').val() > 0 || $('#4').val() > 0 || $('#5').val() > 0 || $('#6').val() > 0 || $('#7').val() > 0 || $('#8').val() > 0 || $('#9').val() > 0 || $('#10').val()){
		    tmpdata = $('#receipt').serialize();
		    clearElements();
		    $.ajax({
		      type: 'post',
		      url: 'post.php',
		      data: tmpdata,
		      complete:(function(data) {
		      	if(data.responseText == "-1"){
		      		alert("Draw is already completed.");
		      		window.location.reload();
		      	}else if(data.responseText == "0"){
		      		alert("No Balance");
		      	}else{
		           		//window.open('ticket.php?id='+data.responseText,'_blank');
		           		$("#iframeDiv").html('');
			           	$('#iframeDiv').append("<iframe src='ticket.php?id="+data.responseText+"'></iframe>");
		        }
		        getBalance();
		      })
		    });		  
		  }else{
		  	alert("Please enter quantity.");
			  }
			}else{
		  	alert("Draw is not available.");
			  }
		  }else{
      	alert("No Balance");
		  }
		}
  });
  
  $('#buy').on('click', function () {
    $('#receipt').submit();
  });

});

$(function () {
$( document ).on( 'keydown', function ( e ) {
	    //console.log("asd"+e.keyCode);
//		alert(e.keyCode );
	    if ( e.keyCode === 27 ) { // ESC
	       if($("#centerPopup").is(':visible')){
	       $("#centerPopup").hide();
	       getCurrentData(1);
		     }
	    } else if ( e.keyCode === 120 ) { // F9
	       $('#can').click();
	    } else if ( e.keyCode === 117 ) { // F6
	       $('#receipt').submit();
	    } else if ( e.keyCode === 119 ) { // F6
	       $('#scancode').focus();
	    } else if ( e.keyCode === 116 ) { // F6
	       clearElements();
	    } else if ( e.keyCode === 118 ) { // F7
	       window.open('luckydraw.php', '_blank');
	    } else {
	    	// 
	    }
	});
	$('body').on('click', '#closeDiv', function() {
      $("#centerPopup").hide();
      getCurrentData(1);
	});
	
	$('#CurrentTime, #slider1_container, #Righttop, #yantra_content').on('click', function (e) {
    $("#1").focus();
  });

	$("#1").focus();
});

$(".onlynum").keydown(function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
        (e.keyCode == 65 && e.ctrlKey === true) || 
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        return;
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

// Execute on body load and display current time on RHS
/*setInterval(function(){
	var Xdate = new Date();
	Xtime = Math.floor(Xdate.getTime());
	JPDate = Xtime + (SLDiff * 1000);
		
	var adate = new Date();
	adate.setTime(JPDate);
	aHours = adate.getHours();
	aMinutes = adate.getMinutes();
	aSeconds = adate.getSeconds();
	aHours = ((aHours + 11) % 12 + 1);
	if(aHours < 10) aHours = "0" + aHours;
	if(aMinutes < 10) aMinutes = "0" + aMinutes;
	if(aSeconds == 3)
		getResult();
	if(aSeconds < 10) aSeconds = "0" + aSeconds;
	$('#currentTimer').html(aHours + ":" + aMinutes + ":" + aSeconds); 
	
}, 1000);
*/
var LeftSideTime;

// Execute when click on Current Button
function getCurrentData(isupcomming){
	if(isupcomming == 1)
		qStr = "act=loadData";
	else
		qStr = "act=loadData&FullDate="+$("#currentFullDateTime").html();
	$.ajax({
		url:"ajax.php",
		data:qStr,
		type:"POST",
		datatype:"json",
		success:function(data){
			data = JSON.parse(data);
			if(data.draw_id != ""){
				$("#draw_id").val(data.draw_id);
				$("#currentFullDateTime").html(data.currentFullDateTime);
				$("#ctime").html(data.currentTime);
				clearInterval(LeftSideTime);
//				LeftSideTime = setInterval(startLeftTime,1000);
			}
		},
		error:function(){
			clearInterval(LeftSideTime);
			//alert("Error while loading try again");
		}
	});
}

// Execute when click on UpComing Button
function UpComing(){
	$.ajax({
		url:"ajax.php",
		data:"act=upcoming",
		type:"POST",
		datatype:"json",
		success:function(data){
			data = JSON.parse(data);
			
/*			var Ydate = new Date();
			Ytime = Math.floor(Ydate.getTime());
			JPDate = Ytime + (SLDiff * 1000);
*/						
			/*var date = new Date();
			date.setTime(JPDate)
			*/
			$("#centerPopupInner").html('');
			olddate = "";
			$.each(data, function(index, element) {
					currentTime = element.currentTime;
					i = currentTime.substring(0, 5);
					newdate = element.currentFullDateTime.substring(0, 10);
					if(olddate != newdate){
						$('#centerPopupInner').append('<div class="currenDate">'+ newdate.substring(3, 5) + "/" + newdate.substring(0, 2) + "/" + newdate.substring(6, 10) + '</div>');
					}
          $('#centerPopupInner').append('<span><input type="button" name="t'+i+'" value="'+element.currentTime+'" datacurrentFullDateTime="'+element.currentFullDateTime+'" datadrawid="'+element.draw_id+'" /></span>');
          olddate = newdate;
          //console.log(olddate);
      });
      
      $('#centerPopupInner input').addClass("centerPopupinput");
			var overlay = $("#centerPopup");
			top = $(window).height() - ($("#centerPopup").height() / 2);
			left = -(overlay.outerWidth() / 2);
			overlay.css({'margin-top': top,'margin-left': left+180});
			if(olddate != "")	$('#centerPopup').show();
			
		},
		error:function(){
			//alert("Error while loading try again");
		}
	});
}

// Get result data
function getResult(){
	$.ajax({
		url:"ajax.php",
		data:"act=getresult&FullTime="+$("#ctime").html(),
		type:"POST",
		datatype:"json",
		success:function(data){
			data = JSON.parse(data);
      $('#content').html('<div id="innerContent"><ul></ul></div>');
			var list = $('#content').find('ul');
			is_lastYantraID = 0;
			$.each(data, function(index, element) {
				imgNo = parseInt(element.win_product_id);
				if(imgNo > 0){
					if(imgNo < 10) newImgNo = "0"+imgNo; else newImgNo = imgNo;
				}else{
					newImgNo = "";
				}
				
				if(element.is_last == '1'){
					newId = ' id="lastYantraID"';
					is_lastYantraID++;
				}else{
					newId = '';
				}
				
				if(element.is_jackpot == 'YES')
					list.append('<li'+newId+'><div class="yantralist jp"><div class="lefttext"><div class="lefttime">'+element.currentTime+'</div><div class="leftstart"><img src="./images/star.gif" style="width:35px !important" height="30px" /></div></div><div class="rightyantra jackport">'+((newImgNo!="")?'<img src="./images/'+imageLoad+'GW'+newImgNo+'.jpg" />':'')+'</div></div></li>');
				else
					list.append('<li'+newId+'><div class="yantralist"><div class="lefttext"><div class="lefttime">'+element.currentTime+'</div><div class="leftstart"></div></div><div class="rightyantra">'+((newImgNo!="")?'<img src="./images/'+imageLoad+'GW'+newImgNo+'.jpg" />':'')+'</div></div></li>');

      });
     	
      $('#content').scrollTop(0);
      if(is_lastYantraID > 0){
      parentDivTop = $('#content').scrollTop();
      innerContentHeight = $('#innerContent').innerHeight();
      lastYantraIDHeight = $('#lastYantraID').innerHeight();
      offsetval = $('#lastYantraID').position();
      lastYantraIDPosition = (offsetval.top - 20);
      //console.log(innerContentHeight + " - " + lastYantraIDPosition + " - " + $('#lastYantraID').height());
      $('#content').animate({"scrollTop": lastYantraIDPosition}, "slow");
	    }
      
		},
		error:function(){
			//alert("Error while loading try again");
		}
	});
}

// Get company balance
function getBalance(){
	$.ajax({
		url:"ajax.php",
		data:"act=getBalance",
		type:"POST",
		datatype:"json",
		success:function(data){
			if(data != ''){
      	$('.currentBalance').html(data);
			}
		},
		error:function(){
			//alert("Error while loading try again");
		}
	});
}


// Last Receipt
$('body').on('click', '.lreceipt', function() {	
	window.open('ticket.php?lastrec=true', '_blank');
});

// Purchase Receipt
$('body').on('click', '.pdetail', function() {	
	window.open('purchase.php', '_blank');
});

// Lucky Draw
$('body').on('click', '.luckyyantra', function() {	
	window.open('luckydraw.php', '_blank');
});

// Execute on body load or current button click and start timer above the LHS yantra
/*
function startLeftTime(){
	var Zdate = new Date();
	Ztime = Math.floor(Zdate.getTime());
	JPDate = Ztime + (SLDiff * 1000);
		
	var CurrentDateTime = new Date(); 
	CurrentDateTime.setTime(JPDate);
	EndTimer = new Date($("#currentFullDateTime").html());

	var timeDiff = EndTimer - CurrentDateTime; 
	timeDiff = timeDiff / 1000; 
	var days = Math.floor(timeDiff / 86400); 
	
	var thours = Math.floor(timeDiff / 3600) % 24; 
	tthours = thours;
	if(thours < 10) thours = "0" + thours; 
	
	var tminutes = Math.floor(timeDiff / 60) % 60; 
	ttminutes = tminutes;
	if(tminutes < 10) tminutes = "0" + tminutes; 
	
	var tseconds = Math.floor(timeDiff % 60);
	ttseconds = tseconds;
	if(tseconds < 10) tseconds = "0" + tseconds;
	//console.log(tthours+":"+ttminutes+":"+ttseconds+"-"+Math.floor(timeDiff));
	if(Math.floor(timeDiff) == 5){
		setWinProduct();
	}
	if(Math.floor(timeDiff) == 1){
		//clearInterval(LeftSideTime);
		getCurrentData(0);
		//getResult();
	}
	if(tthours >= 0 && ttminutes >= 0 && ttseconds >= 0){
		$("#ltime").html(thours+":"+tminutes+":"+tseconds);
	}
}
*/
// set Win Product
function setWinProduct(){
	$.ajax({
		url:"ajax.php",
		data:"act=setWinProduct",
		type:"POST",
		datatype:"json",
		success:function(data){},
		error:function(){}
	});
}

// Default call to start timer above the LHS yantra
getCurrentData(0);

// Default call to get result
getResult();

$('body').on('click', '.centerPopupinput', function() {
	$("#centerPopup").hide();
	clearInterval(LeftSideTime);
	$("#draw_id").val($(this).attr("datadrawid"));
	$("#currentFullDateTime").html($(this).attr("datacurrentFullDateTime"));
	//console.log($(this).attr("datacurrentFullDateTime")+ " - " +$(this).attr("datadrawid"));
	$("#ctime").html($(this).val());
	LeftSideTime = setInterval(startLeftTime,1000);
});

$('body').on('keyup', '.udlrClass', function(e) {
	calQtyAmt();
	var thisIndex = parseInt($(this).attr("dataIndex"));
	var thisId = $(this).attr("id");
	var newIndex = null;
	if (e.keyCode == 37) {  //left
		if(thisIndex == 1)
			$("#10").focus().select();
		else
			$("#"+(thisIndex-1)).focus().select();
	}
	if (e.keyCode == 38) { 	//up
		if(thisIndex == 6) $("#1").focus().select();
		if(thisIndex == 7) $("#2").focus().select();
		if(thisIndex == 8) $("#3").focus().select();
		if(thisIndex == 9) $("#4").focus().select();
		if(thisIndex == 10) $("#5").focus().select();
	}
	if (e.keyCode == 39) { 	//right
		if(thisIndex == 10)
			$("#1").focus().select();
		else
			$("#"+(thisIndex+1)).focus().select();
	}
	if (e.keyCode == 40) { 	//down
		if(thisIndex == 1) $("#6").focus().select();
		if(thisIndex == 2) $("#7").focus().select();
		if(thisIndex == 3) $("#8").focus().select();
		if(thisIndex == 4) $("#9").focus().select();
		if(thisIndex == 5) $("#10").focus().select();
	}
});

function calQtyAmt(){
	product_price = $("#product_price").val();
	$("#qty").val('');
	$("#amt").val('');
	qty = 0;
	$('.udlrClass').each(function(){
		if(parseInt($(this).val()) > 0)
	 		qty = qty + parseInt($(this).val());
	});
	$("#qty").val(qty);
	$("#amt").val((qty * product_price));
}

$('#loadWinner').on('submit', function (e) {
  e.preventDefault();
  if($('#scancode').val() > 9999){
	  $.ajax({
			url:"ajax.php",
			data:"act=getwinner&hash_key="+$('#scancode').val(),
			type:"POST",
			success:function(data){
				if(data == 'winner'){
					$("#iframeDiv").html('');
		      $('#iframeDiv').append("<iframe src='winner.php?id="+$('#scancode').val()+"'></iframe>");
				}else if(data == 'cancel'){
					alert("Sorry, Your receipt was cancel.");
				}else if(data == 'scan'){
					alert("Sorry, Your receipt was already scan.");
				}else if(data == 'nowinner'){
					alert("Sorry, Your are not winner.");
				}else if(data == 'pending'){
					alert("Sorry, Draw is still pending.");
				}else if(data == 'noretailer'){
					alert("Sorry, Receipt is not available with this retailer.");
				}else if(data == 'nodraw'){
					alert("Sorry, Draw is not available.");
				}else{
					alert("Sorry, Bar code incorrect.");
				}
				
				$('#scancode').val('');
				getBalance();
			},
			error:function(){
				//alert("Error while loading try again");
				$('#scancode').val('');
			}
		});	  
	}else{
		alert("Sorry, Bar code length incorrect.");
	}
	
});

$('#can').on('click', function (e) {
  e.preventDefault();
  var r = confirm("Are you sure to cancel receipt?");
  if (r == true) {
  $.ajax({
		url:"ajax.php",
		data:"act=canReceipt",
		type:"POST",
		success:function(data){
			if(data == '1'){
				getBalance();
      	alert("Last receipt is cancel.");
			}else
				alert("Sorry, Draw is already done and we can't cancel last receipt.");
		},
		error:function(){
			//alert("Error while loading try again");
		}
	});	  
	}  
});

$('#clear').on('click', function (e) {
  e.preventDefault();
  clearElements();
});


function clearElements(){
	$('#1').val('');
	$('#2').val('');
	$('#3').val('');
	$('#4').val('');
	$('#5').val('');
	$('#6').val('');
	$('#7').val('');
	$('#8').val('');
	$('#9').val('');
	$('#10').val('');
	$('#qty').val('');
	$('#amt').val('');
	$('#scancode').val('');
}