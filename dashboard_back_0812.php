<?php
include_once('includes/basepath.php');
if(!isset($_SESSION['user_id'])){
	header('Location: ./index.php');
	exit;
}
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="js/cycle.js"></script>
	<script type="text/javascript" src="js/cycle.tile.js"></script>
	<script type="text/javascript" src="js/loadtime.js"></script>
</head>
<body>
	<div id="centerPopup"><div class="closeDiv"><input type="button" value="X" class="close" id="closeDiv" /></div><div id="centerPopupInner"></div></div>
	<?php if($company_id == 2){ ?>
	<div class="top">
		<div id="Righttop">
			<span id="currentTimer"></span>
			<lable class="currentBalance">
					<?php
				$sSQL = "SELECT current_balance FROM users WHERE user_id = ".$_SESSION['user_id'];
					$rs = mysql_query($sSQL) or print(mysql_error());
					$row = mysql_fetch_array($rs);
				echo formatAmt($row["current_balance"]);
					?>
			</lable><strong>Hello, <?php echo $_SESSION['username'];?></strong> <a href="logout.php">Logout</a><br />
			<strong class="whiteColor"><?php echo date('d/m/Y');?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P.No. 204011111210
		</div>	
	</div>
	<div id="main_wrapper">        
    <div id="CurrentTime">
			<div class="ctime">Time</div>
			<div class="ltime">Time Left</div>
			<div class="ctime" id="ctime"></div>
			<span id="currentFullDateTime" style="display:none"></span>
			<div class="ltime" id="ltime"></div>
		</div>
    <div id="slider1_container">
      <div class="cycle-slideshow" data-cycle-fx='tileSlide' data-cycle-speed="1000" data-cycle-delay="500" data-cycle-tile-count="10" data-cycle-tile-vertical="true">
				<img src="./images/<?php echo $imageLoad;?>GW01.jpg" />
        <img src="./images/<?php echo $imageLoad;?>GW02.jpg" />
        <img src="./images/<?php echo $imageLoad;?>GW03.jpg" />
        <img src="./images/<?php echo $imageLoad;?>GW04.jpg" />
        <img src="./images/<?php echo $imageLoad;?>GW05.jpg" />
        <img src="./images/<?php echo $imageLoad;?>GW06.jpg" />
        <img src="./images/<?php echo $imageLoad;?>GW07.jpg" />
        <img src="./images/<?php echo $imageLoad;?>GW08.jpg" />
        <img src="./images/<?php echo $imageLoad;?>GW09.jpg" />
        <img src="./images/<?php echo $imageLoad;?>GW10.jpg" />
      </div>
    </div>
    <form id="receipt">
	    <div id="yantra_list">
				<ul>
					<li><img src="./images/<?php echo $imageLoad;?>GW01.jpg" /><input type="text" name="1" class='udlrClass onlynum' dataIndex="1" id="1" /></li>
	        <li><img src="./images/<?php echo $imageLoad;?>GW02.jpg" /><input type="text" name="2" class='udlrClass onlynum' dataIndex="2" id="2" /></li>
	        <li><img src="./images/<?php echo $imageLoad;?>GW03.jpg" /><input type="text" name="3" class='udlrClass onlynum' dataIndex="3" id="3" /></li>
	        <li><img src="./images/<?php echo $imageLoad;?>GW04.jpg" /><input type="text" name="4" class='udlrClass onlynum' dataIndex="4" id="4" /></li>
	        <li><img src="./images/<?php echo $imageLoad;?>GW05.jpg" /><input type="text" name="5" class='udlrClass onlynum' dataIndex="5" id="5" /></li>
	        <li><img src="./images/<?php echo $imageLoad;?>GW06.jpg" /><input type="text" name="6" class='udlrClass onlynum' dataIndex="6" id="6" /></li>
	        <li><img src="./images/<?php echo $imageLoad;?>GW07.jpg" /><input type="text" name="7" class='udlrClass onlynum' dataIndex="7" id="7" /></li>
	        <li><img src="./images/<?php echo $imageLoad;?>GW08.jpg" /><input type="text" name="8" class='udlrClass onlynum' dataIndex="8" id="8" /></li>
	        <li><img src="./images/<?php echo $imageLoad;?>GW09.jpg" /><input type="text" name="9" class='udlrClass onlynum' dataIndex="9" id="9" /></li>
	        <li><img src="./images/<?php echo $imageLoad;?>GW10.jpg" /><input type="text" name="10" class='udlrClass onlynum' dataIndex="10" id="10" /></li>
	      </ul>
	    </div>
			<input type="hidden" name="draw_id" value="1"/>
			<input style="display:none;" name="submit" type="submit" value="Submit">
		</form>
		<div class="clearing"></div>
	</div>
	<?php }else{ ?>
	<div id="mainWrapper">
		<div id="leftArea">
			<?php if($package==2){?>
			<div class="logo"><img src="./images/<?php echo $imageLoad;?>navratan.gif"><div>Pune</div></div>
			<?php }?>
			<div id="CurrentTime">
				<div class="ctime">Time</div>
				<div class="ltime">Time Left</div>
				<div class="ctime" id="ctime"></div>
				<span id="currentFullDateTime" style="display:none"></span>
				<div class="ltime" id="ltime"></div>
			</div>
			<div id="slider1_container">
        <div class="cycle-slideshow" data-cycle-fx='tileSlide' data-cycle-speed="1000" data-cycle-delay="500" data-cycle-tile-count="10" data-cycle-tile-vertical="true">
          <img src="./images/<?php echo $imageLoad;?>GW01.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW02.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW03.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW04.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW05.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW06.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW07.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW08.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW09.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW10.jpg" />
        </div>
    	</div>	
		</div>
		<div id="RightArea">

			<div id="Righttop">
				<span id="currentTimer"></span>
				<lable class="currentBalance">
					<?php
					$sSQL = "SELECT current_balance FROM users WHERE user_id = ".$_SESSION['user_id'];
					$rs = mysql_query($sSQL) or print(mysql_error());
					$row = mysql_fetch_array($rs);
					echo formatAmt($row["current_balance"]);
					?>
				</lable><strong>Hello, <?php echo $_SESSION['username'];?></strong> <a href="logout.php">Logout</a><br />
				<strong class="whiteColor"><?php echo date('d/m/Y');?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P.No. 204011111210
			</div>	
			
      <div id="yantra_list">
      	<form id="receipt">
					<ul>
            <li><img src="./images/<?php echo $imageLoad;?>GW01.jpg" /><input type="text" name="1" class='udlrClass onlynum' dataIndex="1" id="1" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW02.jpg" /><input type="text" name="2" class='udlrClass onlynum' dataIndex="2" id="2" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW03.jpg" /><input type="text" name="3" class='udlrClass onlynum' dataIndex="3" id="3" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW04.jpg" /><input type="text" name="4" class='udlrClass onlynum' dataIndex="4" id="4" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW05.jpg" /><input type="text" name="5" class='udlrClass onlynum' dataIndex="5" id="5" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW06.jpg" /><input type="text" name="6" class='udlrClass onlynum' dataIndex="6" id="6" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW07.jpg" /><input type="text" name="7" class='udlrClass onlynum' dataIndex="7" id="7" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW08.jpg" /><input type="text" name="8" class='udlrClass onlynum' dataIndex="8" id="8" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW09.jpg" /><input type="text" name="9" class='udlrClass onlynum' dataIndex="9" id="9" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW10.jpg" /><input type="text" name="10" class='udlrClass onlynum' dataIndex="10" id="10" /></li>
          </ul>
					<input type="hidden" name="draw_id" id="draw_id" value="0"/>
        	<input style="display:none;" name="submit" type="submit" value="Submit">
				</form>
			</div>
		</div>
		<div class="clearing"></div>
	</div>
	<?php } ?>
	<div id="yantra_content">
    <div id="content"></div>
  </div>
  
  <div id="footer">
  	<?php
  	$sSQL = "SELECT	product_price FROM company WHERE company_id = ".$company_id;
  	$rs = mysql_query($sSQL);
  	$product_price = 0;
  	if(mysql_num_rows($rs) > 0){
  		$row = mysql_fetch_array($rs);
  		$product_price = $row["product_price"];
  	}
  	?>
  	<input type="hidden" name="product_price" id="product_price" value="<?php echo $product_price;?>" />
  	<input type="button" name="yantra" value="Yantra" class="yantra" />
  	<input type="button" name="current" value="Current" class="current" onclick="getCurrentData(1)" />
  	<input type="button" name="upcoming" value="Upcoming" class="upcoming" onclick="UpComing()"/>
  	<input type="text" name="qty" id="qty" value="" class="test1" readonly="readonly" />
  	<input type="text" name="amt" id="amt" value="" class="test2" readonly="readonly" />
  	<span class="barcodetext">F8 Bar Code</span>
  	<form name="loadWinner" id="loadWinner" style="display:initial;">
  	<input type="text" name="scancode" id="scancode" value="" class="barcode" />
  	</form>
  	<br />
  	<input type="button" name="buy" id="buy" value="F6 Buy" class="buy" />
  	<input type="button" name="clear" id="clear" value="F5 Clear" class="clar" />
  	<input type="button" name="can" id="can" value="F9 Can. Rec." class="can" />
  	<input type="button" name="lreceipt" value="Last Receipt" class="lreceipt"/>
  	<input type="button" name="exit" value="Exit" class="exit" />
  	<input type="button" name="purchase" value="Purchase Details" class="pdetail" />
  	<input type="button" name="luckyyantra" value="F7 Lucky Yantras" class="luckyyantra" />
  </div>
  <div id="iframeDiv" style="display:none"></div>
  <script type="text/javascript">
  	imageLoad = "<?php echo $imageLoad; ?>";
  	  	
  	$(document).ready(function() {
  	  	/*
  	var serverTime = "<?php echo time(); ?>";
		var localTime = Math.floor(new Date().getTime() / 1000);
			SLDiff = serverTime - localTime;
		
		var date1 = new Date();
		newTime = Math.floor(date1.getTime());
		JPDate = newTime + (SLDiff * 1000);
		
		var date2 = new Date();
		date2.setTime(JPDate);
		Hours = date2.getHours();
		Hours = ((Hours + 11) % 12 + 1);
		Minutes = date2.getMinutes();
		Seconds = date2.getSeconds();
		console.log(Hours + ":" + Minutes + ":" + Seconds);

  		*/

  		
    	var script = document.createElement('script');
			script.src = "js/custom.js";

	window.setTimeout(function () {
	        document.getElementsByTagName('body')[0].appendChild(script);
	    }, 1000); // 1 sec
			});
	
  </script>
</body>
</html>
