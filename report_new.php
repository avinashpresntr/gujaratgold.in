<?php
include_once('includes/basepath.php');if(!isset($_SESSION['username'])){	header('Location: index.php');}
$recordsForDate = isset($_GET['date']) ? substr($_GET['date'],0,4)."-".substr($_GET['date'],5,2)."-".substr($_GET['date'],8,2) : date("Y-m-d");
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
	<link rel="stylesheet" href="css/jquery-ui.css" />
	<link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/cycle.js"></script>
	<script type="text/javascript" src="js/cycle.tile.js"></script>
	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>	
	<script language="javascript">
    jQuery(document).ready(function() {
        jQuery('#date').datepicker({ dateFormat: 'yy-mm-dd' });
	})

 	function submit(user_id){
		var url= "report_new.php?user_id="+user_id+"&date="+$('#date').val();
		window.location.replace(url);
    }
  </script>
  <style>
  	th.odd,td.odd { color:red; }
  	th.even,td.even { color:black; }
  	.oddraw { background-color:#DDEBF7; }
  	.evenraw { background-color:#FFFFFF; }
  </style>
</head>

<body>
	<?php include_once('menu.php');?>
	<br /> <br />
	<form name="frm" id ="rp" action="report_new.php" method="get">
	<a class="report" style="margin-left: 10px;" href="#" onclick="submit('0');" >Generate for All Users</a>&nbsp; <b>OR</b>
	<?php
	$sSQL = "SELECT user_id,username FROM users ORDER BY username";
	$rs = mysql_query($sSQL);
	?>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			var availableTags = [ <?php while($row = mysql_fetch_assoc($rs)) { ?> "<?php echo $row['username']; ?>", <?php } ?> ];
			jQuery( "#user_id" ).autocomplete({
				source: availableTags
			});
		});
	</script>
			
	<input type="text" id="user_id" name="user_id" placeholder="Select Username" value="<?php  //echo $user_id;  ?>" style="margin-left: 10px;">
	<input type="text" id="date" name="date" placeholder="Select Date" value="<?php  echo $recordsForDate;  ?>" style="margin-left: 10px;">
	<input type="submit" name="submitBtn" value="Go!">
	<?php
	if(isset($_GET["user_id"]) && trim($_GET["user_id"]) != "" ){
		$user_query = " SELECT user_id FROM `users` WHERE username LIKE '".trim($_GET["user_id"])."'";
		$user_query_res = mysql_query($user_query) or print(mysql_error());
		$user_id = 0;
		if($row = mysql_fetch_array($user_query_res)) {
			$user_id = $row['user_id'];
		}

	}
	if(isset($_GET["user_id"]) && trim($_GET["user_id"]) != "" ){
		?>
		<?php if($user_id !== 0 ) { ?>
		Current result for: <?php echo $_GET["user_id"] ?>
		<?php } else {
		?>
		Current result for: All users
		<?php
		}
	}
	?>
	</form>	
	
	<div id="mainWrapper" style="margin-top:20px;">
		<div class="box-body table-responsive">
				<table width="80%" align="center" cellpadding="4" cellspacing="0" border="1" class="table table-bordered table-striped">
					<thead>
						<tr>
						  <th rowspan="2" width="10%">Draw Id</th>
							<th rowspan="2" width="20%">Draw DateTime</th>
							<th colspan="11">Product</th>                                               
							<th colspan="4">&nbsp;</th>
						</tr>
						<tr>                                                
							<th width="5%" class="odd">1</th><th width="5%" class="even">2</th>
							<th width="5%" class="odd">3</th><th width="5%" class="even">4</th>
							<th width="5%" class="odd">5</th><th width="5%" class="even">6</th>
							<th width="5%" class="odd">7</th><th width="5%" class="even">8</th>
							<th width="5%" class="odd">9</th><th width="5%" class="even">10</th>
							<th width="5%">Total</th>
							<th width="5%">Win</th>
							<th width="5%">&nbsp;</th>
							<th width="5%">Percent</th>
							<th width="5%">Draw wise winning</th>
						</tr>
					</thead>
					<tbody>                  
					<?php
					$products = array();
					$totalArray = array();
					for($i=0;$i<10;$i++)
					{
						$products[$i] = $i+1; 
						$totalArray[$i] = 0;
					}
					$totalArray[10] = 0;
					$totalArray[11] = 0;
					$m=0;
					$qry = "SELECT * FROM draw
					        WHERE drawdatetime LIKE '".$recordsForDate."%'
					        ORDER BY drawdatetime";
					//$qry = "SELECT * FROM draw WHERE draw_id >= 3868 AND draw_id < 3870 ORDER BY drawdatetime";
					
					$res = mysql_query($qry) or print(mysql_error());											
					$nums = mysql_num_rows($res);
					$c1 = "evenraw";

					$ProductPriceSql = "SELECT product_price FROM company where company_id = '" . $company_id . "' LIMIT 1";
					$ProductPriceRS = mysql_query($ProductPriceSql);
					$ProductPriceRow = mysql_fetch_object($ProductPriceRS);
					$product_price = isset($ProductPriceRow->product_price)?$ProductPriceRow->product_price:10;


					while($row = mysql_fetch_array($res)) {												
						if($c1 == "evenraw") $c1 = "oddraw"; else $c1 = "evenraw";
						$totalProducts = 0;
						if($nums != 0) {
							$drawid = $row['draw_id'];
							$win_product_qty = 0;
							echo "<tr class='".$c1."'><td>".$drawid."</td>";
							$time = date("d-m-Y h:i:s",strtotime($row['drawdatetime']));										
							echo "<td>".$time."</td>";
							$qry2 = "SELECT * FROM receipt_master where draw_id='$drawid' AND receipt_cancel = 0 ";
							if(isset($_GET["user_id"]) && trim($_GET["user_id"]) != "" && $user_id != 0){
								$qry2 = $qry2 . " AND retailer_id = ".trim($user_id);
							}
							$res2 = mysql_query($qry2) or print(mysql_error());
							$num_rows = mysql_num_rows($res2);
							if($num_rows != 0)
							{
								$rec_details_id = array();
								while($row2 = mysql_fetch_array($res2))
								{	
									$receiptid = $row2['receipt_id'];
									$recid[] = $receiptid;
									
									$qry3 = "SELECT * FROM receipt_details where receipt_id='$receiptid'";												
									$res3 = mysql_query($qry3) or print(mysql_error());
									while($row3 = mysql_fetch_array($res3))
									{									
										$productid[] = $row3['product_id'];
										$quantity[] = $row3['quantity'];
										$rec_details_id[] = $row3['receipt_details_id'];														
									}												
								}											
								
								if(is_array($rec_details_id) && count($rec_details_id) > 0)
									$recdetails = implode(",",$rec_details_id);		
								else
									$recdetails = 0;

								for($i=0;$i<10;$i++)
								{		
									if(($i%2) == 0) $c = "odd"; else $c = "even";
									$qry4 = "SELECT COUNT(product_id) as cnt,product_id,SUM(quantity) as quantity FROM receipt_details where product_id='$products[$i]' and receipt_details_id IN ($recdetails) ";
									$res4 = mysql_query($qry4) or print(mysql_error());
									$rows4 = mysql_fetch_array($res4);
									if($rows4['cnt'] != 0)											
									{														
										echo "<td align='right' class='".$c."'>".$rows4['quantity']."</td>";	
										$totalProducts += $rows4['quantity'];
										$totalArray[$i] = $totalArray[$i] + $rows4['quantity'];
										if($row['win_product_id'] == $products[$i]){
											$win_product_qty = $win_product_qty + $rows4['quantity'];
										}
									}
									else
									{
										echo "<td align='right' class='".$c."'>0</td>";
									}
								}
								$totalArray[10] = $totalArray[10] + $totalProducts;
							}
							else
							{
								for($i=0;$i<10;$i++)
								{	
									if(($i%2) == 0) $c = "odd"; else $c = "even";
									echo "<td align='right' class='".$c."'>0</td>";
								}
							}									

							if($allow_twowin):
								$totalArray[11] = $totalArray[11] + $row['win_product_id'] . ", " . $row['win_product_id2'];
							else:
								$totalArray[11] = $totalArray[11] + $row['win_product_id'];
							endif;
							
							echo "<td align='right'>".$totalProducts."</td>";
							if($allow_twowin):
								echo "<td align='right'>".$row['win_product_id'].(($row['win_product_id2'] != '')?",".$row['win_product_id2']:"")."</td>";
							else:
								echo "<td align='right'>".$row['win_product_id']."</td>";
							endif;
							echo "<td align='right'>".$row['is_jackpot']."</td>";
							echo "<td align='right'>".$row['percent']."</td>";
							$win_amount = $row['win_amount'];
							$company_draw_wise_win_amount = ($totalProducts * $product_price) - ($win_amount * $win_product_qty);
							echo "<td align='right'>".$company_draw_wise_win_amount."</td>";

							unset($recdetails);
							unset($rec_details_id);
							unset($productid);
							unset($quantity);
							$m++;										
						}
						else
						{
							for($i=0;$i<10;$i++)
							{	
								if(($i%2) == 0) $c = "odd"; else $c = "even";
								echo "<td align='right' class='".$c."'>0</td>";
							}
						}
					}
					
					?>
					</tbody>
					<tfoot>
						<tr>                                                
							<th colspan="2" align="right">Total</th>
							<?php
							//if($allow_twowin) $j=10; else $j=11;
							$j=10;
							for($i=0;$i<=$j;$i++) {	
								if(($i%2) == 0 && $i != $j) $c = "odd"; else $c = "even";
								echo "<th align='right' class='".$c."'>".$totalArray[$i]."</th>";
							}
							?>
							<th colspan="4"></th>
						</tr>
					</tfoot>
				</table>
			</div><!-- /.box-body -->
	
	</div> 
</body>
</html>