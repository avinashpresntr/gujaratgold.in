<?php

include_once('includes/basepath.php');if(!isset($_SESSION['username'])){	header('Location: index.php');}

$toDate = isset($_POST['to_date']) ? substr($_POST['to_date'],0,4)."-".substr($_POST['to_date'],5,2)."-".substr($_POST['to_date'],8,2) : date("Y-m-d");

$forDate = isset($_POST['from_date']) ? substr($_POST['from_date'],0,4)."-".substr($_POST['from_date'],5,2)."-".substr($_POST['from_date'],8,2) : date("Y-m-d");

?>

<html>

<head>

	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />

	<link rel="stylesheet" type="text/css" href="css/custom.css" />

	<link rel="stylesheet" href="css/jquery-ui.css" />

	<link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>

	<script src="js/jquery-ui.js"></script>

	<script type="text/javascript" src="js/cycle.js"></script>

	<script type="text/javascript" src="js/cycle.tile.js"></script>

	<!-- DATA TABES SCRIPT -->

	<script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

	<script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>	

	<script language="javascript">

    jQuery(document).ready(function() {

        jQuery('#from_date').datepicker({ dateFormat: 'yy-mm-dd' }); 

        jQuery('#to_date').datepicker({ dateFormat: 'yy-mm-dd' });

        //.datepicker("setDate", new Date())

    })

  </script>

</head>

<?php

$result = array();

if(isset($_POST) && !empty($_POST))

{

	$post_data = $_POST;

	$result = get_data($post_data);

	//echo "<pre>";print_r($result);exit;

} 

?>

<body>

	<?php include_once('menu.php');?>

	<br />

	<br />

	<form name="frm" action="balance_type.php?btype=<?php echo $_REQUEST["btype"]; ?>" method="post">

		<input type="hidden" name="btype" id="btype" value="<?php echo $_REQUEST["btype"]; ?>" />

		Select Retailer : <select name="user_id">

			<option value="">All Retailer</option>

			<?php

			$sSQL = "SELECT user_id,username FROM users ORDER BY username";

			$rs = mysql_query($sSQL);

			if(mysql_num_rows($rs) > 0)

			{

				while($row = mysql_fetch_array($rs))

				{

					if(isset($_POST["user_id"]) && $row["user_id"] == $_POST["user_id"]) $a = ' selected'; else $a = '';

					echo '<option value="'.$row["user_id"].'" '.$a.'>'.$row["username"].'</option>';

				}

			}

			?>

		</select>

		From Date

		<input type="text" id="from_date" name="from_date" placeholder="Select Date" value="<?php  echo $forDate;  ?>">

		To Date

		<input type="text" id="to_date" name="to_date" placeholder="Select Date" value="<?php  echo $toDate;  ?>">

		Summary : 

		<select name="cmbSummary">

			<option value="full"<?php echo (isset($_POST["cmbSummary"]) && $_POST["cmbSummary"]=='full')?" selected=\"selected\"":""; ?>>Full Summary</option>

			<option value="half"<?php echo (isset($_POST["cmbSummary"]) && $_POST["cmbSummary"]=='half')?" selected=\"selected\"":""; ?>>Half Summary</option>

			<option value="detail"<?php echo (isset($_POST["cmbSummary"]) && $_POST["cmbSummary"]=='detail')?" selected=\"selected\"":""; ?>>Detail Summary</option>

		</select>

    	<input type="submit" name="submitBtn" value="Go">

	</form>	

	<div id="mainWrapper" style="margin-top:20px;">

		<div class="box-body table-responsive">

			<?php

			if(isset($_POST["cmbSummary"]) && $_POST["cmbSummary"]=='full')

			{

				include_once("balance_type_full_summary.php"); 

			}

			if(isset($_POST["cmbSummary"]) && $_POST["cmbSummary"]=='half')

			{

				include_once("balance_type_half_summary.php"); 

			}

			if(isset($_POST["cmbSummary"]) && $_POST["cmbSummary"]=='detail')

			{

				include_once("balance_type_detail_summary.php"); 

			}

			?>

		</div><!-- /.box-body -->

	</div> 

</body>

</html>

<?php

	function get_data($data){

		$users = array();

		$usr_qry = "SELECT u.user_id,u.username,rm.user_commission, rm.draw_id, rm.receipt_id FROM users as u LEFT JOIN receipt_master as rm ON u.user_id = rm.retailer_id WHERE is_active = '1'";

		if($data['user_id'] != "")

		{

			$usr_qry .= " AND u.user_id = {$data['user_id']}";

		}

		$usr_qry .= " ORDER BY u.username";

		$usr_qry_rs = mysql_query($usr_qry);

		if(mysql_num_rows($usr_qry_rs) > 0){

			$key = 0;

			while($usr_row = mysql_fetch_array($usr_qry_rs)){

				$transaction = array();

				$credit = get_transaction($usr_row['user_id'], $data['from_date'], $data['to_date'],'Credit');

				$debit = get_transaction($usr_row['user_id'], $data['from_date'], $data['to_date'],'Debit');

				if(!empty($credit))

				{

					foreach($credit as $key => $value){

						$transaction[] = $value;

					}

				}

				if(!empty($debit))

				{

					foreach($debit as $key => $value){

						$transaction[] = $value;

					}

				}

				usort($transaction,"array_sort");

				$users[$key] = $usr_row;

				$users[$key]['transaction'] = $transaction;

				$key++;

			}

		}

		return $users;

	}

	

	function get_transaction($retailer_id,$fromdate,$todate,$flage)

	{

		$trans = array();

		//$trns_qry = "SELECT *, COUNT(amount) as {$flage} FROM transaction WHERE cr_dr = '{$flage}' AND retailer_id = {$retailer_id} AND (DATE(transaction_time) BETWEEN '{$fromdate}' AND '{$todate}') GROUP BY DATE(transaction_time) ORDER BY transaction_time";
		$trns_qry = "SELECT * FROM transaction WHERE cr_dr = '{$flage}' AND retailer_id = {$retailer_id} AND (DATE(transaction_time) BETWEEN '{$fromdate}' AND '{$todate}')ORDER BY transaction_time";
		$trns_qry_rs = mysql_query($trns_qry);

		if(mysql_num_rows($trns_qry_rs) > 0){

			$key = 0;

			while($trns_row = mysql_fetch_array($trns_qry_rs)){

				$trans[$key]['cr_dr'] = $trns_row['cr_dr'];

				$trans[$key]['amount'] = $trns_row['amount'];

				$trans[$key]['transaction_time'] = date("Y-m-d",strtotime($trns_row['transaction_time']));

				$key++;

			}

		}

		return $trans;

	}

	

	function array_sort($a,$b)

	{

		if ($a['transaction_time'] == $b['transaction_time']) {

	        return 0;

	    }

	    return ($a['transaction_time'] < $b['transaction_time']) ? -1 : 1;

	}

?>



