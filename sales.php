<?php
include_once('includes/basepath.php');if(!isset($_SESSION['username'])){	header('Location: index.php');}
$toDate = isset($_POST['to_date']) ? substr($_POST['to_date'],0,4)."-".substr($_POST['to_date'],5,2)."-".substr($_POST['to_date'],8,2) : date("Y-m-d");
$forDate = isset($_POST['from_date']) ? substr($_POST['from_date'],0,4)."-".substr($_POST['from_date'],5,2)."-".substr($_POST['from_date'],8,2) : date("Y-m-d");
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
	<link rel="stylesheet" href="css/jquery-ui.css" />
	<link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/cycle.js"></script>
	<script type="text/javascript" src="js/cycle.tile.js"></script>
	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>	
	<script language="javascript">
    jQuery(document).ready(function() {
        jQuery('#from_date').datepicker({ dateFormat: 'yy-mm-dd' });
        jQuery('#to_date').datepicker({ dateFormat: 'yy-mm-dd' });
        //.datepicker("setDate", new Date())
    })
  </script>
</head>
<?php
$result = array();
if(isset($_POST) && !empty($_POST))
{
	$post_data = $_POST;
	//$result = get_data($post_data);
} 
?>
<body>
	<?php include_once('menu.php');?>
	<br />
	<br />
	<form name="frm" action="sales.php" method="post">
		Select Retailer : <select name="user_id">
			<?php
			$sSQL = "SELECT user_id,username FROM users ORDER BY username";
			$rs = mysql_query($sSQL);
			if(mysql_num_rows($rs) > 0)
			{
				while($row = mysql_fetch_array($rs))
				{
					if(isset($_POST["user_id"]) && $row["user_id"] == $_POST["user_id"]) $a = ' selected'; else $a = '';
					echo '<option value="'.$row["user_id"].'" '.$a.'>'.$row["username"].'</option>';
				}
			}
			?>
		</select>
		From Date
		<input type="text" id="from_date" name="from_date" placeholder="Select Date" value="<?php  echo $forDate;  ?>">
		To Date
		<input type="text" id="to_date" name="to_date" placeholder="Select Date" value="<?php  echo $toDate;  ?>">
    	<input type="submit" name="submitBtn" value="Go">
	</form>	
	<div id="mainWrapper" style="margin-top:20px;">
		<div class="box-body table-responsive">
			<?php
			if(isset($_POST["user_id"]))
			{
				include_once("sales_summary.php"); 
			}
			?>
		</div><!-- /.box-body -->
	</div> 
</body>
</html>
