<?php
$startTime = time();

include_once('./includes/basepath.php');

$echoAllowed = 0;
$select = "SELECT * FROM draw "
        . " WHERE drawdatetime <= '".date('Y-m-d H:i:s')."' "
        . "   AND  (win_product_id IS NULL OR win_product_id = 0)"
        . " ORDER BY drawdatetime";
omEcho($select);
$rs = mysql_query($select);


$win_amount_cmp1 = "SELECT draw_amount FROM company where company_id = '" . $company_id . "'";
$win_amount_rs1 = mysql_query($win_amount_cmp1) or print(mysql_error());
$new_value1 = array();
while ($win_amount_row1 = mysql_fetch_row($win_amount_rs1)) {
	$drawvalue = $win_amount_row1;
}

while($row = mysql_fetch_array($rs)){
  //if($row["is_jackpot"] == 'YES') $win_Amount = 200; else $win_Amount = 100; 
  if($row["is_jackpot"] == 'YES') $win_Amount = $row["win_amount"]; else $win_Amount = $drawvalue[0]; 

  $upperAmounts = array();
  // $upperAmountsQtys = array();
  $lowerAmounts = array();
  // $lowerAmountsQtys = array();
  omEcho("Draw Id = " . $row["draw_id"]);

  $sSQL = "SELECT SUM(quantity * product_price) AS totalAmount FROM receipt_details "
          . " WHERE receipt_id IN (SELECT receipt_id FROM receipt_master "
          . "                       WHERE draw_id = ".$row["draw_id"].")";
  $rs1 = mysql_query($sSQL);
  if($row1 = mysql_fetch_array($rs1)){
    omEcho("totalAmount = " . $row1['totalAmount']);

    if(!($row1['totalAmount'] > 0))
      $makeWinnerId = rand(1,10);
    else {
      $shareAmount =   $row1["totalAmount"] * $row['percent'] / 100; //100 is to find percentage
      $shareQuantity = $shareAmount / $win_Amount;
      omEcho("shareAmount = " . $shareAmount);
      omEcho("shareQuantity = " . $shareQuantity);

      $maxQty = 0;
      $minQty = 0;
      $makeWinnerId = 0;
      $minQtyProduct_id = 0;

      $productQuery = "SELECT * FROM product ORDER BY product_id";
      $productQueryResult = mysql_query($productQuery);

      while($productQueryRow = mysql_fetch_array($productQueryResult)){
        $sSQL = "SELECT SUM(quantity) AS productQty FROM receipt_details "
                . " WHERE product_id = ".$productQueryRow['product_id']." "
                . "   AND receipt_id IN (SELECT receipt_id FROM receipt_master WHERE draw_id = ".$row["draw_id"].")";
        $rs1 = mysql_query($sSQL);
        while($row1 = mysql_fetch_array($rs1)){
          if($row1["productQty"] == "")
            $productQty = 0;
          else
            $productQty = $row1["productQty"];

          if($productQty > $shareQuantity) {
            if(array_key_exists($productQty, $upperAmounts)) {
              array_push($upperAmounts[$productQty], $productQueryRow['product_id']);
            } else {
              $upperAmounts[$productQty] = array($productQueryRow['product_id']);
            }
          } else {
            if(array_key_exists($productQty, $lowerAmounts)) {
              array_push($lowerAmounts[$productQty], $productQueryRow['product_id']);
            } else {
              $lowerAmounts[$productQty] = array($productQueryRow['product_id']);
            }
          }
        }
      }
    }
  }

  omEcho("UPPER ARRAY");
  // omEcho(var_dump($upperAmounts));
  omEcho("LOWER ARRAY");
  // omEcho(var_dump($lowerAmounts));


  if($makeWinnerId == 0) {
    if(count($lowerAmounts) > 0) {
      omEcho("Searching in lower");
      $maxQty = max(array_keys($lowerAmounts));
      $productIds = $lowerAmounts[$maxQty];
      $makeWinnerId = $productIds[array_rand($productIds)];
    } else {
      omEcho("Searching in upper");
      $minQty = min(array_keys($upperAmounts));
      $productIds = $upperAmounts[$minQty];
      $makeWinnerId = $productIds[array_rand($productIds)];
    }
  }


  omEcho("makeWinnerId = ".$makeWinnerId);
  $updateQuery = "UPDATE draw SET win_product_id = ".$makeWinnerId.", win_amount = ".$win_Amount." WHERE draw_id = ".$row["draw_id"];
  mysql_query($updateQuery);
  omEcho($updateQuery);
}

if($scanRequire == 0){
  $select = "SELECT * FROM draw WHERE drawdatetime < '".date('Y-m-d H:i:s')."' ORDER BY drawdatetime";
  $rs = mysql_query($select);
  $winTime = date('Y-m-d H:i:s');
  while($row = mysql_fetch_array($rs)){
    $sSQL = "SELECT receipt_id,retailer_id FROM receipt_master where receipt_scan = 0 AND draw_id = ".$row['draw_id'];
    $rs2 = mysql_query($sSQL) or print(mysql_error());
    while($row2 = mysql_fetch_array($rs2)){
      $sSQL = "SELECT quantity FROM receipt_details WHERE product_id = ".$row['win_product_id']." AND receipt_id = ".$row2["receipt_id"];
      $rs3 = mysql_query($sSQL) or print(mysql_error());
      if(mysql_num_rows($rs3) > 0){
        $row3 = mysql_fetch_array($rs3);
        mysql_query("UPDATE users SET current_balance = current_balance + ".($row3["quantity"]*$row['win_amount'])." WHERE user_id = ".$row2["retailer_id"]);
      }
      mysql_query("UPDATE receipt_master SET receipt_scan = 1,scan_time='".$winTime."' WHERE receipt_id = ".$row2["receipt_id"]);
    
      if($allow_twowin && !is_null($row['win_product_id2'])){
        $sSQL = "SELECT quantity FROM receipt_details WHERE product_id = ".$row['win_product_id2']." AND receipt_id = ".$row2["receipt_id"];
        $rs3 = mysql_query($sSQL) or print(mysql_error());
        if(mysql_num_rows($rs3) > 0){
          $row3 = mysql_fetch_array($rs3);
          mysql_query("UPDATE users SET current_balance = current_balance + ".($row3["quantity"]*$row['win_amount2'])." WHERE user_id = ".$row2["retailer_id"]);
        }
      }
    }
  } 
}

function omEcho($text){
  global $echoAllowed;
  if($echoAllowed == 1){
    echo "<BR/ >".$text;
  }
}

file_put_contents("test.txt", "[" . $startTime . " to " . time() . "] - CRON RUN \n", FILE_APPEND);
?>
