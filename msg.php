<?php if(isset($_SESSION['error'])){?>
<div class="alert alert-danger alert-dismissable">
  <i class="fa fa-ban"></i>
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
  <b>Error!</b> 
	<?php  echo $_SESSION['error'];
	unset($_SESSION['error']);?>
</div>
<?php  }?>
<?php if(isset($_SESSION['success'])){?>
<div class="alert alert-success alert-dismissable">
  <i class="fa fa-check"></i>
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
  <b>Alert!</b> 
	<?php  echo $_SESSION['success'];
	unset($_SESSION['success']);?>
</div>
<?php  }?>