-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 15, 2017 at 08:44 PM
-- Server version: 5.5.52-cll-lve
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gujaratgold`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `draw_amount` double DEFAULT '0',
  `jackput_amount` double DEFAULT '0',
  `draw_amount2` double DEFAULT '0',
  `jackput_amount2` double DEFAULT '0',
  `jp2_amount` double DEFAULT NULL,
  `jp3_amount` double DEFAULT NULL,
  `jp4_amount` double DEFAULT NULL,
  `jp5_amount` double DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `product_price`, `draw_amount`, `jackput_amount`, `draw_amount2`, `jackput_amount2`, `jp2_amount`, `jp3_amount`, `jp4_amount`, `jp5_amount`) VALUES
(1, 'GUJARAT GOLD VAPI', '10', 100, 200, 0, 0, NULL, NULL, NULL, NULL),
(2, 'NAV RATANAM', '10', 100, 200, 0, 0, NULL, NULL, NULL, NULL),
(3, 'Gujarat Gold Lucky Draw', '10', 100, 200, 300, 400, 200, 300, 400, 500);

-- --------------------------------------------------------

--
-- Table structure for table `data_sheet`
--

CREATE TABLE IF NOT EXISTS `data_sheet` (
  `data_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `ds_name` varchar(222) NOT NULL,
  `ds_all_trades` double NOT NULL,
  `ds_long_trades` double NOT NULL,
  `ds_short_trades` double NOT NULL,
  `do_date` datetime NOT NULL,
  `do_buy_sell` varchar(222) NOT NULL,
  `do_price` double NOT NULL,
  `do_qty` double NOT NULL,
  `do_current_value` double NOT NULL,
  `dt_brokerage_fees` varchar(222) NOT NULL,
  `dt_entry_date` varchar(222) NOT NULL,
  `dt_exit_date` varchar(222) NOT NULL,
  `dt_type` varchar(222) NOT NULL,
  `dt_no_bars` varchar(222) NOT NULL,
  `dt_absolute_performance` double NOT NULL,
  `dt_brokerage` double NOT NULL,
  `hl_file_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=745 ;

-- --------------------------------------------------------

--
-- Table structure for table `draw`
--

CREATE TABLE IF NOT EXISTS `draw` (
  `draw_id` int(11) NOT NULL AUTO_INCREMENT,
  `drawdatetime` datetime DEFAULT NULL,
  `win_product_id` int(11) DEFAULT NULL,
  `win_amount` double NOT NULL DEFAULT '0',
  `is_jackpot` enum('NO','YES') NOT NULL,
  `win_product_id2` int(11) DEFAULT NULL,
  `win_amount2` double NOT NULL DEFAULT '0',
  `is_jackpot2` enum('NO','YES') NOT NULL,
  `percent` double NOT NULL DEFAULT '0',
  `last_update_time` datetime DEFAULT NULL,
  `last_update_ip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`draw_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2005 ;

--
-- Dumping data for table `draw`
--


-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `company_id`, `product_name`) VALUES
(1, 2, 'One'),
(2, 2, 'Two'),
(3, 2, 'Three'),
(4, 2, 'Four'),
(5, 2, 'Five'),
(6, 2, 'Six'),
(7, 2, 'Seven'),
(8, 2, 'Eight'),
(9, 2, 'Nine'),
(10, 2, 'Ten');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_details`
--

CREATE TABLE IF NOT EXISTS `receipt_details` (
  `receipt_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`receipt_details_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1116 ;

--
-- Dumping data for table `receipt_details`
--

-- --------------------------------------------------------

--
-- Table structure for table `receipt_master`
--

CREATE TABLE IF NOT EXISTS `receipt_master` (
  `receipt_id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_time` datetime NOT NULL,
  `receipt_ip` varchar(15) DEFAULT NULL,
  `retailer_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `draw_id` int(11) NOT NULL,
  `hash_key` varchar(13) DEFAULT NULL,
  `receipt_cancel` tinyint(4) NOT NULL DEFAULT '0',
  `cancel_time` datetime DEFAULT NULL,
  `cancel_ip` varchar(15) DEFAULT NULL,
  `receipt_scan` tinyint(4) NOT NULL DEFAULT '0',
  `user_commission` double NOT NULL DEFAULT '0',
  `win_time` datetime DEFAULT NULL,
  `win_ip` varchar(15) DEFAULT NULL,
  `scan_time` datetime DEFAULT NULL,
  PRIMARY KEY (`receipt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=251 ;

--
-- Dumping data for table `receipt_master`
--

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `retailer_id` int(11) DEFAULT NULL,
  `cr_dr` varchar(255) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `trans_for_id` int(11) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `transaction_time` datetime DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `transaction`
--

-- --------------------------------------------------------

--
-- Table structure for table `trans_for`
--

CREATE TABLE IF NOT EXISTS `trans_for` (
  `trans_for_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_for` varchar(255) NOT NULL,
  PRIMARY KEY (`trans_for_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `current_balance` double NOT NULL,
  `usertype` int(11) NOT NULL COMMENT '0=admin,1=retailer, 2=agent',
  `user_commission` double NOT NULL DEFAULT '0',
  `user_ip` varchar(15) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `online` datetime DEFAULT NULL,
  `city` varchar(15) DEFAULT NULL,
  `note` text,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10038 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `parent_id`, `username`, `password`, `current_balance`, `usertype`, `user_commission`, `user_ip`, `is_active`, `online`, `city`, `note`) VALUES
(1, 0, 'admin', '21232f297a57a5a743894a0e4a801fc3', 47581, 0, 0, '', 1, '2017-01-15 23:13:26', NULL, NULL),
(30, 0, 'omtest', '30dd1ab18633e1f914b95db672cb6450', 48849.5, 0, 5, NULL, 1, '2017-01-16 09:00:03', NULL, NULL),
(10031, 0, 'gold6', '4a4314ef967aad20a9e7c423bc16e39c', 5000, 1, 0, NULL, 1, NULL, '', ''),
(10030, 0, 'gold5', 'b53b3a3d6ab90ce0268229151c9bde11', 5000, 1, 0, NULL, 1, NULL, '', ''),
(10029, 0, 'gold4', 'ae11976937537e4c1206237dea035331', 5000, 1, 0, NULL, 1, NULL, '', ''),
(10028, 0, 'gold3', '745e9cdc76ffef9a8c1ae15e3f2e8cf9', 5000, 1, 0, NULL, 1, '2017-01-13 09:06:46', '', ''),
(10027, 0, 'gold2', '2d387ab98437e5f528a87b031a086256', 4600, 1, 0, NULL, 1, '2017-01-15 09:46:37', '', ''),
(10026, 0, 'gold1', '0120a4f9196a5f9eb9f523f31f914da7', 4450, 1, 0, NULL, 1, '2017-01-15 17:15:41', '', ''),
(10025, 0, 'jayesh', 'e10adc3949ba59abbe56e057f20f883e', 5374.850000000001, 1, 5, NULL, 1, '2017-01-04 11:32:27', 'Rajkot', ''),
(10024, 0, 'rameshbhai', 'e10adc3949ba59abbe56e057f20f883e', 2736.5000000000027, 1, 10, NULL, 1, '2017-01-08 16:12:27', 'rajkot - Gujrat', ''),
(10032, 1, 'agent1', '486dac28e554a94e65feb6bf5acd98bc', 25000, 2, 0, NULL, 1, '2017-01-15 14:07:01', '', ''),
(10033, 10032, 'a1gold1', '98ea8da977be9e2b9fa25bce709b4799', 5000, 1, 0, NULL, 1, NULL, '', ''),
(10034, 10032, 'a1gold2', '66738d96b4715a8615a1109f27068416', 5000, 2, 0, NULL, 1, NULL, '', ''),
(10035, 10032, 'a1gold3', 'e1223695638fcca42425f1bfb843e489', 5003, 1, 0, NULL, 1, NULL, '', ''),
(10036, 1, 'Jigo', 'cb57cdb7cc459dc6fbbc33f91485b5e2', 5000, 2, 0, NULL, 1, NULL, '', ''),
(10037, 10036, 'Kekado', 'f6c0c55d8d0a5cd1f69ceaf3d5b4e108', 0, 1, 0, NULL, 1, '2017-01-15 20:57:17', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users_login`
--

CREATE TABLE IF NOT EXISTS `users_login` (
  `users_login_id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `login_time` datetime NOT NULL,
  `user_ip` varchar(15) DEFAULT NULL,
  `valid_invalid` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_login`
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
