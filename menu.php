<?php
if($_SESSION['usertype'] == 0)
{
?>
<nav id="menu" class="menu">
  <ul class="topnav" id="myTopnav">
      <li class="hidden-fullscreen site-title-menu"><a href="javascript:void(0);" target="_blank">Gujarat Gold</a></li>
      <li><a href="dashboard.php" target="_blank">Dashboard</a></li>
      <li><a href="dashboard_mobile.php" target="_blank">Mobile</a></li>
      <li><a href="dashboard_mobile3.php" target="_blank">Mobile3</a></li>
            <!--<li><a href="mdashboard.php" target="_blank">Mobile</a></li>-->

    <li class="has-submenu">
      <a href="javascript:void(0);">Entry</a>
      <ul>
        <li><a href="addretailer.php">Create Retailer</a></li>
        <li><a href="transaction.php?cr_dr=Credit">Add</a></li>
        <li><a href="transaction.php?cr_dr=Debit">Less</a></li>
        <li><a href="per.php">Percentage</a></li>
        <li><a href="perNew.php">New Percentage</a></li>
      </ul>
    </li>
    <li class="has-submenu">
      <a href="javascript:void(0);">User Reports</a>
      <ul>
        <li><a href="win1.php">Win</a></li>
        <li><a href="win2.php">Win 2</a></li>
        <li><a href="summaryOfBalance.php">Balance Summary</a></li>
        <li><a href="report_new.php">Drawwise Purchase</a></li>
        <li><a href="report2.php">Scan Report</a></li>
        <li><a href="balance_type.php?btype=1">Type 1</a></li>
        <li><a href="balance_type.php?btype=2">Net Points Report</a></li>
        <li><a href="balance_type.php?btype=3">Type 3</a></li>
        <li><a href="sales.php">Sales Report</a></li>
      </ul>
    </li>
    <li class="has-submenu">
      <a href="javascript:void(0);">Om</a>
      <ul>
        <li><a href="adddrawdate.php">Add Drawdate</a></li>
        <li><a href="report.php">Report</a></li>
        <li><a href="viewtransaction.php">View Transaction</a></li>
        <li><a href="ledger.php">Ledger</a></li>
        <li><a href="ledger_new.php">New Ledger</a></li>
        <li><a href="report22.php">Report2 Old</a></li>
        <li><a href="users.php">Retailer IP</a></li>
        <li><a href="user_login.php">Login Report</a></li>
        <li><a href="perdraw.php">Per</a></li>
      </ul>
    </li>
    <li><a href="profile.php">Change Password</a></li>
    <li><a href="logout.php">Logout</a></li>
      <li class="icon site-title-menu">
          <a href="javascript:void(0);" onclick="myFunction()">MENU</a>
      </li>
</ul>
</nav>

<?php
}
else if($_SESSION['usertype'] == 2)
{
?>

<nav id="menu" class="menu">
  <ul class="topnav" id="myTopnav">
      <li class="hidden-fullscreen site-title-menu"><a href="javascript:void(0);" target="_blank">Gujarat Gold</a></li>
      <li><a href="dashboard_mobile3.php" target="_blank">Mobile</a></li>
      <li class="has-submenu">
        <a href="javascript:void(0);">Entry</a>
        <ul>
          <li><a href="addretailer.php">Create User</a></li>
          <li><a href="transaction.php?cr_dr=Credit">Add</a></li>
          <li><a href="transaction.php?cr_dr=Debit">Less</a></li>
          <li><a href="summaryOfBalance.php">Balance Summary</a></li>
        </ul>
      </li>
      <li><a href="profile.php">Change Password</a></li>
      <li><a href="logout.php">Logout</a></li>
      <li class="icon site-title-menu">
          <a href="javascript:void(0);" onclick="myFunction()">MENU</a>
      </li>
  </ul>
</nav>

<?php
}
?>

<link href="css/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css" />
<script type="text/javascript" src="js/tinynav.js"></script>
<script>
    $(document).ready(function(){
        /*$(document).on('hide',"li.icon",function(){
            $('.responsive > li.has-submenu').each(function(){
                $(this).find('ul').css('position','absolute');
            });
        }*/
        $(document).on('click',"li.has-submenu",function(){
            $('.responsive > li.has-submenu').each(function(){
                $(this).find('ul').css('position','absolute');
            });
            if($('li.icon').is(":visible")){
                if($(this).closest('ul.responsive').length > 0){
                    $(this).find('ul').css('position','relative');
                }
            }
        });
    });
$(function () {
  $("#menu").tinyNav();
});
/* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}
</script>
