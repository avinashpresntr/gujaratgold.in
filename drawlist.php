<?php
include_once('includes/basepath.php');
if(!isset($_SESSION['user_id'])){
	header('Location: ./index.php');
	exit;
}
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" /> 
  <script type="text/javascript" src="js/jquery-ui.js"></script>  
  
  <link rel="stylesheet" type="text/css" href="js/plugins/datatablereponsive/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="js/plugins/datatablereponsive/extensions/Responsive/css/dataTables.responsive.css">
	<script type="text/javascript" language="javascript" src="js/plugins/datatablereponsive/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" src="js/plugins/datatablereponsive/extensions/Responsive/js/dataTables.responsive.js"></script>
</head>
<body>
	<div class="mainwrapper" style="width:80%; margin:0px auto; padding:10px; text-align:center">
	 <b>Draw Result</b>
	 <table id="example" class="display responsive nowrap" cellspacing="0" width="100%">
    	<thead>
        <tr>
            <th>No.</th>
            <th>Daw Time</th>
            <th>Bought Qty</th>
            <th>Points</th>
            <th>Win Qty</th>
            <th>Win Points</th>
        </tr>
    	</thead>
    	<tbody>
    		<?php
    		$product_price = 0;
				$sSQL = "SELECT product_price FROM company WHERE company_id = ".$company_id;
				$rs = mysql_query($sSQL);
				if(mysql_num_rows($rs) > 0){
					$row = mysql_fetch_array($rs);
					$product_price = $row["product_price"];
				}
				
				$currentdate = date('Y-m-d');
    		$i = 1;
    		$sSQL = "SELECT *,DATE_FORMAT(drawdatetime,'%h:%i %p') AS TIME_FORMATTED,STR_TO_DATE(drawdatetime,'%Y-%m-%d') AS DATE_FORMATTED FROM draw having DATE_FORMATTED = '".$currentdate."' ORDER BY drawdatetime";
    		$rs = mysql_query($sSQL) or print(mysql_error());
    		while($row = mysql_fetch_array($rs)){
    			
    			$FinalQty = $FinalAmount = $FinalWin1Qty = $FinalWin2Qty = $FinalWinAmount = 0;
    			$totalQty = $totalAmount = $totalWin1Qty = $totalWin2Qty = $totalWinAmount = 0;
    			$sSQL = "SELECT * FROM receipt_master where receipt_cancel = 0 AND draw_id = ".$row['draw_id']." AND retailer_id = ".$_SESSION['user_id'];
				  $rs1 = mysql_query($sSQL) or print(mysql_error());
				  while($row1 = mysql_fetch_array($rs1)){
				  	
				  	$sSQL = "SELECT SUM(quantity) AS totalQty from receipt_details where receipt_id = ".$row1["receipt_id"];
				  	$rs2 = mysql_query($sSQL) or print(mysql_error());
	          $row2 = mysql_fetch_array($rs2);
	          if(!is_null($row2['totalQty'])) $totalQty = $row2['totalQty']; else $totalQty = 0;
	          $FinalQty = $FinalQty + $totalQty;
	          $FinalAmount = $FinalAmount + $totalQty * $product_price;

				  	if($row['win_product_id'] != ""){
					  	$sSQL = "SELECT SUM(quantity) AS totalWinQty from receipt_details where receipt_id = ".$row1["receipt_id"]." AND product_id = ".$row['win_product_id'];
					  	$rs3 = mysql_query($sSQL) or print(mysql_error());
		          $row3 = mysql_fetch_array($rs3);
		          if(!is_null($row3['totalWinQty'])) $totalWin1Qty = $row3['totalWinQty']; else $totalWin1Qty = 0;
		          $FinalWin1Qty = $FinalWin1Qty + $totalWin1Qty;
		          $FinalWinAmount = $FinalWinAmount + ($totalWin1Qty*$row['win_amount']);
	        	}
	        	
	        	$totalWin2Qty = 0;
	        	if($allow_twowin && $row['win_product_id2'] != ""):
	        		$sSQL = "SELECT SUM(quantity) AS totalWinQty from receipt_details where receipt_id = ".$row1["receipt_id"]." AND product_id = ".$row['win_product_id2'];
					  	$rs4 = mysql_query($sSQL) or print(mysql_error());
		          $row4 = mysql_fetch_array($rs4);
		          if(!is_null($row4['totalWinQty'])) $totalWin2Qty = $row4['totalWinQty']; else $totalWin2Qty = 0;
		          $FinalWin2Qty = $FinalWin2Qty + $totalWin2Qty;
		          $FinalWinAmount = $FinalWinAmount + ($totalWin2Qty*$row['win_amount2']);
	        	endif;
				  }
    		?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row["TIME_FORMATTED"]; ?></td>
            <td><?php echo $FinalQty; ?></td>
            <td><?php echo $FinalAmount; ?></td>
            <td><?php echo $FinalWin1Qty+$FinalWin2Qty; ?></td>
            <td><?php echo $FinalWinAmount; ?></td>
        </tr>
        <?php
        $i++;
        }
        ?>
    	</tbody>
	</table>
<script>
$(document).ready(function() {
    $('#example').DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false,
        "bFilter":	false
    });
} );
</script>
<style>
table.dataTable thead th, table.dataTable tfoot th { text-align:left; }
</style>
</div>
</body>
</html>

