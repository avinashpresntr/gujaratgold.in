<?php
include_once('includes/basepath.php');if(!isset($_SESSION['username'])){	header('Location: index.php');}

$recordsForDate = isset($_POST['date']) ? substr($_POST['date'],0,4)."-".substr($_POST['date'],5,2)."-".substr($_POST['date'],8,2) : date("Y-m-d");
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
        <link rel="stylesheet" href="css/jquery-ui.css" />
	 <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
        <script src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/cycle.js"></script>
	<script type="text/javascript" src="js/cycle.tile.js"></script>
	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>	
  <script language="javascript">
    jQuery(document).ready(function() {
        jQuery('#date').datepicker({ dateFormat: 'yy-mm-dd' });
        //.datepicker("setDate", new Date())
    })
  </script>
</head>

<body>
	<?php include_once('menu.php');?>
	<br />
	<br />
  <form name="frm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <input type="text" id="date" name="date" placeholder="Select Date" 
           value="<?php  echo $recordsForDate;  ?>">
		Select Retailer : <select name="user_id" onchange="frm.submit();">
			<option value="0">All</option>
			<?php
			$sSQL = "SELECT user_id,username FROM users WHERE usertype = 1 ORDER BY username";
			$rs = mysql_query($sSQL);
			if(mysql_num_rows($rs) > 0){
			  $userCount = 0;
				while($row = mysql_fetch_array($rs)){
				  $userArray[$userCount]['user_id'] = $row["user_id"];
				  $userArray[$userCount]['username'] = $row["username"];
				  
					if(isset($_POST["user_id"]) && $row["user_id"] == $_POST["user_id"]) $a = ' selected'; else $a = '';
					echo '<option value="'.$row["user_id"].'" '.$a.'>'.$row["username"].'</option>';
					
					$userCount++;
				}
			}
			?>
		</select>
    <input type="submit" name="submitBtn" value="Display !">
	</form>	
	
  <table cellpadding="3" cellspacing="0" border="1">
<?php	
	$qry = "SELECT * FROM draw
	        WHERE drawdatetime LIKE '".$recordsForDate."%'
	        ORDER BY drawdatetime";
	$res = mysql_query($qry) or print(mysql_error());											
	$nums = mysql_num_rows($res);
	while($row = mysql_fetch_array($res))
	{
	  echo "<tr>";
    echo "<td>".$row['drawdatetime']."</td>";
	  echo "</tr>";
	  for($i = 0;$i < $userCount; $i++)
	  {
      if(isset($_REQUEST['user_id']))
      {
        if($_REQUEST['user_id'] == 0)
          $displayMe = true;
        else if($_REQUEST['user_id'] == $userArray[$i]['user_id'])
          $displayMe = true;
        else
          $displayMe = false;
      }
      else
        $displayMe = true;
      
      if($displayMe)
      {
        if($row['win_product_id'] == 1 || $row['win_product_id'] == 2 || $row['win_product_id'] == 3 || $row['win_product_id'] == 4 || 
           $row['win_product_id'] == 5 || $row['win_product_id'] == 6 || $row['win_product_id'] == 7 || $row['win_product_id'] == 8 || 
           $row['win_product_id'] == 9 || $row['win_product_id'] == 10)
        {
          echo "<tr><td NOWRAP>".$userArray[$i]['username']. " : ";
          $countQuery = "SELECT COUNT(quantity) AS totalWinQty FROM receipt_details
                          WHERE product_id = ".$row['win_product_id']."
                            AND receipt_id IN (SELECT receipt_id FROM receipt_master
                                                WHERE draw_id        = ".$row['draw_id']."
                                                  AND retailer_id    = ".$userArray[$i]['user_id']."
                                                  AND receipt_cancel = 0)";
          $countResult = mysql_query($countQuery);
          $countRow    = mysql_fetch_array($countResult);
          echo "Total Win : ".$countRow['totalWinQty']. ". ";
    //      ====
          $countQueryScan = "SELECT COUNT(quantity) AS ScanQty FROM receipt_details
                          WHERE product_id = ".$row['win_product_id']."
                            AND receipt_id IN (SELECT receipt_id FROM receipt_master
                                                WHERE draw_id        = ".$row['draw_id']."
                                                  AND retailer_id    = ".$userArray[$i]['user_id']."
                                                  AND receipt_cancel = 0
                                                  AND receipt_scan   = 1)";
          $countResultScan = mysql_query($countQueryScan);
          $countRowScan    = mysql_fetch_array($countResultScan);
          echo "Scan : ".$countRowScan['ScanQty']. ". ";
    //      ====
          $countQueryNotScan = "SELECT COUNT(quantity) AS NotScanQty FROM receipt_details
                          WHERE product_id = ".$row['win_product_id']."
                            AND receipt_id IN (SELECT receipt_id FROM receipt_master
                                                WHERE draw_id        = ".$row['draw_id']."
                                                  AND retailer_id    = ".$userArray[$i]['user_id']."
                                                  AND receipt_cancel = 0
                                                  AND receipt_scan   = 0)";
          $countResultNotScan = mysql_query($countQueryNotScan);
          $countRowNotScan    = mysql_fetch_array($countResultNotScan);
          echo "NotScan : ".$countRowNotScan['NotScanQty']. ". ";
          echo "</td></tr>";
          
    //      ====
          $displayQueryNotScan = "SELECT * FROM receipt_master
                                                WHERE draw_id        = ".$row['draw_id']."
                                                  AND retailer_id    = ".$userArray[$i]['user_id']."
                                                  AND receipt_cancel = 0
                                                  AND receipt_scan   = 0
                                                  AND receipt_id IN (SELECT receipt_id AS NotScanQty FROM receipt_details
                                                                      WHERE product_id = ".$row['win_product_id'].")";
          $displayResultNotScan = mysql_query($displayQueryNotScan);
          while($displayRowNotScan    = mysql_fetch_array($displayResultNotScan))
          {
            echo "<tr><td NOWRAP>";
            echo "NotScan : ".$displayRowNotScan['hash_key']. ". ";
            echo "</td></tr>";
          }
        }
      }
	  }
	}												
?>
  </table>

</body>
</html>

