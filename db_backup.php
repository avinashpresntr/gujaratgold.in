<?php
	function __backup_mysql_database($params)
	{
		
		$mtables = array(); $contents = "-- Database: `".$params['db_to_backup']."` --\n";
		
		$mysqli = new mysqli($params['db_host'], $params['db_uname'], $params['db_password'], $params['db_to_backup']);
		if ($mysqli->connect_error) {
			die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
		}

		$results = $mysqli->query("SHOW TABLES");
		
		while($row = $results->fetch_array()){
			if (!in_array($row[0], $params['db_exclude_tables'])){
				$mtables[] = $row[0];
			}
		}

		foreach($mtables as $table){
			$contents .= "-- Table `".$table."` --\n";
			
			$results = $mysqli->query("SHOW CREATE TABLE ".$table);
			while($row = $results->fetch_array()){
				$contents .= $row[1].";\n\n";
			}

			$results = $mysqli->query("SELECT * FROM ".$table);
			$row_count = $results->num_rows;
			$fields = $results->fetch_fields();
			$fields_count = count($fields);
			
			$insert_head = "INSERT INTO `".$table."` (";
			for($i=0; $i < $fields_count; $i++){
				$insert_head  .= "`".$fields[$i]->name."`";
					if($i < $fields_count-1){
							$insert_head  .= ', ';
						}
			}
			$insert_head .=  ")";
			$insert_head .= " VALUES\n";        
					
			if($row_count>0){
				$r = 0;
				while($row = $results->fetch_array()){
					if(($r % 400)  == 0){
						$contents .= $insert_head;
					}
					$contents .= "(";
					for($i=0; $i < $fields_count; $i++){
						$row_content =  str_replace("\n","\\n",$mysqli->real_escape_string($row[$i]));
						
						switch($fields[$i]->type){
							case 8: case 3:
								$contents .=  $row_content;
								break;
							default:
								$contents .= "'". $row_content ."'";
						}
						if($i < $fields_count-1){
								$contents  .= ', ';
							}
					}
					if(($r+1) == $row_count || ($r % 400) == 399){
						$contents .= ");\n\n";
					}else{
						$contents .= "),\n";
					}
					$r++;
				}
			}
		}
		
		if (!is_dir ( $params['db_backup_path'] )) {
				mkdir ( $params['db_backup_path'], 0777, true );
		 }
		
		$backup_file_name = "sql-backup-".date( "d-m-Y--h-i-s").".sql";
			 
		$fp = fopen($backup_file_name ,'w+');
		if (($result = fwrite($fp, $contents))) {
			//echo "Backup file created '--$backup_file_name' ($result)"; 
		}
		fclose($fp);

		/*attach generated sql file and send email to $params['email_address'], then unlink that file*/
		$file = $params['db_backup_path'].$backup_file_name;
		$filename = $backup_file_name;

		$mailto = $params['email_address'];
		$subject = 'db backup of gujaratgold.in';
		$message = 'PFA of db file';
		$content = file_get_contents($file);
		$content = chunk_split(base64_encode($content));


		// a random hash will be necessary to send mixed content
		$separator = md5(time());

		// carriage return type (RFC)
		$eol = "\r\n";

		// main header (multipart mandatory)
		$headers = "From: Omviryash <omviryash@gmail.com>" . $eol;
		$headers .= "MIME-Version: 1.0" . $eol;
		$headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
		$headers .= "Content-Transfer-Encoding: 7bit" . $eol;
		$headers .= "This is a MIME encoded message." . $eol;

		// message
		$body = "--" . $separator . $eol;
		$body .= "Content-Type: text/plain; charset=\"iso-8859-1\"" . $eol;
		$body .= "Content-Transfer-Encoding: 8bit" . $eol;
		$body .= $message . $eol;

		// attachment
		$body .= "--" . $separator . $eol;
		$body .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
		$body .= "Content-Transfer-Encoding: base64" . $eol;
		$body .= "Content-Disposition: attachment" . $eol;
		$body .= $content . $eol;
		$body .= "--" . $separator . "--";

		/*echo "<pre>";
		echo $body;
		die;*/
		//SEND Mail
		if (mail($mailto, $subject, $body, $headers)) {
			if(file_exists($file)){
				unlink($file);
			}
			echo "mail send ... OK"; // or use booleans here
		} else {
			echo "mail send ... ERROR!";
			print_r( error_get_last() );
		}
	}

	$para = array(
		'db_host'=> 'localhost',  //mysql host
		'db_uname' => 'root',  //user
		'db_password' => '', //pass
		'db_to_backup' => 'gujaratgold', //database name
		'db_backup_path' => './', //where to backup
		'db_exclude_tables' => array(), //tables to exclude,
		'email_address' => 'omviryash@gmail.com'
	);
	__backup_mysql_database($para);
