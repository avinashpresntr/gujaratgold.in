<?php
include_once('includes/basepath.php');
if(!isset($_SESSION['user_id'])){
	header('Location: ./index.php');
	exit;
}
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="js/cycle.js"></script>
	<script type="text/javascript" src="js/cycle.tile.js"></script>
	<script type="text/javascript">
	isNetwork();
	 setInterval(function(){
		 isNetwork();
	 },10000);
	    
	 $('#network').show();
	 function isNetwork(){
		 $.ajax({
			    url: 'index.php',
			    type: 'GET',
			    contentType: "application/json;charset=utf-8",
			    success: function (data) {
			        // Yor success logic  
			    	$('#network').show();
			        $('#nonetwork').hide();
			    
			    },
			    error: function (request) {
			        if(request.status==0){
				        // Yor success logic  
				    	$('#network').hide();
				        $('#nonetwork').show();
			        }
			    }
			});
	 }
         
	</script>
</head>

<body>
	<div id="centerPopup">
      <div class="closeDiv">
          <input type="button" value="X" class="close" id="closeDiv" />
      </div>
      <div id="centerPopupInner"></div>
  </div>
	<div id="mainWrapper">
		<div id="leftArea">
			<?php if($package == 2): ?>
			<div class="logo">
	    	<img src="images/navratan.gif">
	      <div>Pune</div>
	    </div>
	  	<?php endif; ?>
			<div id="CurrentTime">
				<div class="ctime">Time</div>
				<div class="ltime">Time Left</div>
				<div class="ctime" id="ctime"></div>
				<span id="currentFullDateTime" style="display:none"></span>
				<div class="ltime<?php if($package == 3) echo ' timeleft'; ?>" id="ltime"></div>
			</div>
			<div id="slider1_container">
				<?php if($package == 3): ?>
				<img src="./images/<?php echo $imageLoad;?>3d.gif" />
				<?php else: ?>
        <div class="cycle-slideshow" data-cycle-fx='tileSlide' data-cycle-speed="1000" data-cycle-delay="500" data-cycle-tile-count="10" data-cycle-tile-vertical="true">
          <img src="./images/<?php echo $imageLoad;?>GW01.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW02.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW03.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW04.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW05.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW06.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW07.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW08.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW09.jpg" />
	        <img src="./images/<?php echo $imageLoad;?>GW10.jpg" />
        </div>
      	<?php endif; ?>
    	</div>	
		</div>
		<div id="RightArea">
			<div id="Righttop">
				<?php if($package == 3): ?>
					<div class="rightinfo">
						<div class="logininfo" id="">
							<img src="./images/smily_1.png" id="network" style="display: none;">
							<img src="./images/nonetwork.png" id="nonetwork" style="display: none;">
							</div>
						<div id="currentTimer" class="curTime"></div>
						<div class="prcblkinfo">
							<div class="prccls"><lable class="currentBalance"><?php
							$sSQL = "SELECT current_balance FROM users WHERE user_id = ".$_SESSION['user_id'];
							$rs = mysql_query($sSQL) or print(mysql_error());
							$row = mysql_fetch_array($rs);
							echo round($row["current_balance"]);
							?></lable></div>
							<div class="dtcls"><?php echo date('d/m/Y');?></div>
						</div>
						<div class="cln"></div>
						<div class="pnno">P.No. 204011111210</div>
					</div>
					<div class="clngap"></div>
				<?php else: ?>
					<span id="currentTimer"></span>
					<lable class="currentBalance">
						<?php
						$sSQL = "SELECT current_balance FROM users WHERE user_id = ".$_SESSION['user_id'];
						$rs = mysql_query($sSQL) or print(mysql_error());
						$row = mysql_fetch_array($rs);
						echo formatAmt($row["current_balance"]);
						?>
					</lable><strong>Hello, <?php echo $_SESSION['username'];?></strong> <a href="logout.php">Logout</a><br />
					<strong class="whiteColor"><?php echo date('d/m/Y');?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P.No. 204011111210
				<?php endif; ?>
			</div>	
			
      <div id="yantra_list">
      	<?php if($package==3 && $company_id==3 ){?>
<style>
	#yantra_list ul li input:focus {
	    /*background-color: #1975FF;*/
	    color: #000;
	}			
</style>
<?php }?>      	
      	<form id="receipt">
					<ul>
            <li><img src="./images/<?php echo $imageLoad;?>GW01.jpg" /><input type="text"  name="1" class='udlrClass onlynum' dataIndex="1" id="1" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW02.jpg" /><input type="text" name="2" class='udlrClass onlynum' dataIndex="2" id="2" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW03.jpg" /><input type="text" name="3" class='udlrClass onlynum' dataIndex="3" id="3" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW04.jpg" /><input type="text" name="4" class='udlrClass onlynum' dataIndex="4" id="4" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW05.jpg" /><input type="text" name="5" class='udlrClass onlynum' dataIndex="5" id="5" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW06.jpg" /><input type="text" name="6" class='udlrClass onlynum' dataIndex="6" id="6" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW07.jpg" /><input type="text" name="7" class='udlrClass onlynum' dataIndex="7" id="7" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW08.jpg" /><input type="text" name="8" class='udlrClass onlynum' dataIndex="8" id="8" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW09.jpg" /><input type="text" name="9" class='udlrClass onlynum' dataIndex="9" id="9" /></li>
		        <li><img src="./images/<?php echo $imageLoad;?>GW10.jpg" /><input type="text" name="10" class='udlrClass onlynum' dataIndex="10" id="10" /></li>
          </ul>
					<input type="hidden" name="draw_id" id="draw_id" value="0"/>
        	<input style="display:none;" name="submit" type="submit" value="Submit">
				</form>
			</div>
		</div>
		<div class="clearing"></div>
	</div>
	
	<div id="yantra_content">
    <div id="content"></div>
  </div>
  
  <div id="footer">
  	<?php
  	$sSQL = "SELECT	product_price FROM company WHERE company_id = ".$company_id;
  	$rs = mysql_query($sSQL);
  	$product_price = 0;
  	if(mysql_num_rows($rs) > 0){
  		$row = mysql_fetch_array($rs);
  		$product_price = $row["product_price"];
  	}
  	?>
  	<?php if($package == 3): ?>
  	<table width="100%" cellspacing="0" cellpadding="0" border="0">
  		<tr>
  			<td align="right" colspan="3" >
  	<?php endif; ?>
  	<input type="hidden" name="product_price" id="product_price" value="<?php echo $product_price;?>" />
  	<input type="button" name="yantra" value="Yantra" class="yantra" />
  	<input type="button" name="drawlist" value="List Draw" class="drawlist" />
  	<input type="button" name="current" value="Current" class="current" onClick="getCurrentData(0)" />
  	<input type="button" name="upcoming" value="Upcoming" class="upcoming" onClick="UpComing()"/>
  	<input type="text" name="qty" id="qty" value="" class="test1" readonly="readonly" />
  	<input type="text" name="amt" id="amt" value="" class="test2" readonly="readonly" />
  	<span class="barcodetext">F8 -<br>Bar Code</span>
  	<form name="loadWinner" id="loadWinner" style="display:initial;">
  	<input type="text" name="scancode" id="scancode" value="" class="barcode" />
  	</form>
  	<?php if($package == 3): ?>
  			</td>
  		</tr>	
  		<tr>
  			<td align="left">
  	<?php else: ?>
  	<br />
  	<?php endif; ?>
  	<input type="button" name="buy" id="buy" value="F6 Buy" class="buy" />
  	<input type="button" name="clear" id="clear" value="F5 Clear" class="clar" />
  	<input type="button" name="can" id="can" value="F9 Can. Rec." class="can" />
  	<input type="button" name="lreceipt" value="Last Receipt" class="lreceipt"/>
  	<?php if($package == 3): ?>
  			</td>
  			<td align="center">
  	<?php endif; ?>
  	<input type="button" name="exit" value="Exit" class="exit" />
  	<?php if($package == 3): ?>
  			</td>
  			<td align="right">
  	<?php endif; ?>
  	<input type="button" name="purchase" value="Purchase Details" class="pdetail" />
  	<input type="button" name="luckyyantra" value="F7 Lucky Yantras" class="luckyyantra" />
  	<?php if($package == 3): ?>
  			</td>
  		</tr>
  	</table>
  	<?php endif; ?>
  </div>
  <div id="iframeDiv" style="display:none"></div>
  <script type="text/javascript">
  	imageLoad = "<?php echo $imageLoad; ?>";
  	allow_twowin = "<?php echo $allow_twowin; ?>";
  	cpackage = "<?php echo $package; ?>";
  </script>
</body>
<script type="text/javascript" src="js/custom2.js"></script>
</html>
