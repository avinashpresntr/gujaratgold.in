<table id="example1" cellpadding="4" cellspacing="0" border="1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th width="10%">Sl. No.</th>
            <th width="20%">Date</th>
            <th width="15%">Bought</th>
            <th width="25%">Commission</th>
            <th width="25%">Difference</th>
        </tr>
    </thead>
    <tbody>
<?php
$tbought=$tcomm=$tdiff=0;
$retailerId=$_POST['user_id'];
$slno=1;
    $sqlprev="SELECT DATE_FORMAT(receipt_master.receipt_time,'%d %m %Y') as vdate, Sum(receipt_details.quantity*receipt_details.product_price) AS bought, Sum(receipt_details.quantity*receipt_details.product_price*receipt_master.user_commission/100) AS comm, Sum(receipt_details.quantity*receipt_details.product_price)-Sum(receipt_details.quantity*receipt_details.product_price*receipt_master.user_commission/100) AS diff
    FROM users INNER JOIN (receipt_master INNER JOIN receipt_details ON receipt_master.receipt_id = receipt_details.receipt_id) ON users.user_id = receipt_master.retailer_id
    WHERE (((users.user_id)='".$retailerId."')) AND ((DATE(receipt_master.receipt_time) Between '".$_POST['from_date']."' AND '".$_POST['to_date']."'))
    GROUP BY DATE_FORMAT(receipt_master.receipt_time,'%d %m %Y')";
$resprev=mysql_query($sqlprev);
while($qFetch = mysql_fetch_array($resprev)){
    ?>
        <tr>
            <td><?php echo $slno;?></td>
            <td><?php echo $qFetch["vdate"];?></td>
            <td><?php echo formatAmt($qFetch["bought"]);?></td>
            <td><?php echo formatAmt($qFetch["comm"]);?></td>
            <td><?php echo formatAmt($qFetch["diff"]);?></td>
        </tr>
<?php
    $tbought+=$qFetch["bought"];
    $tcomm+=$qFetch["comm"];
    $tdiff+=$qFetch["diff"];
    $slno+=1;
}
?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2">Total</td>
            <td><?php echo formatAmt($tbought);?></td>
            <td><?php echo formatAmt($tcomm);?></td>
            <td><?php echo formatAmt($tdiff);?></td>
        </tr>
    </tfoot>
</table>