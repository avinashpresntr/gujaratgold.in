<?php
session_start();
date_default_timezone_set("Asia/Kolkata");
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

//get the current script name
$script_name = explode('/', $_SERVER['PHP_SELF']);
$script_name = end($script_name);

$resaller_access_pages = array(
                            'index.php', 
                            'dashboard_mobile3.php', 
                            'logout.php', 
														'loadtime.php', 
                            'ajax.php',
                            'ticket.php',
                            'purchase.php',
                            'luckydraw.php',
                            'post.php',
                            'winner.php',
                            'profile.php'
                            );
if(isset($_SESSION['usertype']) && $_SESSION['usertype'] == 1 && !in_array($script_name, $resaller_access_pages))
{
  header("Location: dashboard_mobile3.php");
}

$agent_access_pages = array(
                            'index.php', 
                            'dashboardadmin.php', 
                            'addretailer.php', 
                            'transaction.php', 
                            'dashboard_mobile3.php', 
                            'logout.php', 
														'loadtime.php', 
                            'ajax.php',
                            'ticket.php',
                            'purchase.php',
                            'luckydraw.php',
                            'post.php',
                            'winner.php',
                            'profile.php',
                            'summaryOfBalance.php'
                            );
if(isset($_SESSION['usertype']) && $_SESSION['usertype'] == 2 && !in_array($script_name, $agent_access_pages))
{
  header("Location: dashboardadmin.php");
}

//if(!isset($_SESSION['usertype']) && $script_name != 'index.php' && $script_name != 'adddrawdate.php')
  //header("Location: index.php");
  
define("ADMIN_IP_RESTRICTION", FALSE);
define("USER_IP_RESTRICTION", FALSE);  
  
$host = "localhost"; //database location
$user = "root"; //database username
$pass = ""; //database password

//$user = "root"; //database username
//$pass = ""; //database password

$dbName = "gujaratgold"; //database name

$link = mysql_connect($host, $user, $pass);//database connection
mysql_select_db($dbName);

//$baseUrl="http://www.gujaratgold.in/";
$baseUrl="http://localhost/gujaratgold.in/";

$imgPath="images/";

$yantra = array("Kartik Yantra","Gajanand Yantra","Gangotri Yantra","Bajrang Yantra","Bhairav Yantra","Ridhi Yantra","Sava Yantra","Suraj Yantra","Sidhi Yantra","Vastu Yantra");

$package = 3;
$company_id = 3;

$allow_twowin = true;
$printRequire = 1;
$scanRequire = 0;

$allowAdminPassword = true;
$allowAdminActiveDeactive = true;
$allowAdminEdit = true;
$allowAdminDelete = true;


switch ($package){
	case 1:
		$cssLoad = "mainp1";
		$imageLoad = "p1/";
		break;
	case 2:
		$cssLoad = "mainp2";
		$imageLoad = "p1/";
		break;
	case 3:
		$cssLoad = "mainp3";
		$imageLoad = "p3/";
		break;
	default:
		$cssLoad = "main";
		$imageLoad = "";
		break;
}

function formatAmt($amt){
	if(!is_numeric($amt)) $amt = 0;
	return number_format($amt, 2, '.', '');
}

function generateRandomString($length = 8) {
  $characters = '0123456789';
  $randomString = '';
/*   for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, strlen($characters) - 1)];
  }
 */  
  $length = 13;
$alph = substr(str_shuffle("ABCDEFGHIJKLMNPQRSTUVWXYZ"), 0, 2);
$num  = substr(str_shuffle("0123456789987654321"), 0, 12);
$randomString  = substr(str_shuffle($alph.$num), 0, 13);
  
  if(mysql_num_rows(mysql_query("SELECT hash_key FROM receipt_master WHERE hash_key = '".$randomString."'"))<=0)
  	return $randomString;
  else
  	generateRandomString();
}
function rtnretailer($id){
	if(is_numeric($id)){ 
		$sSQL1 = "SELECT username FROM users WHERE user_id = ".$id;
		$rs1 = mysql_query($sSQL1) or print(mysql_error());
		if(mysql_num_rows($rs1) > 0){
			$row1 = mysql_fetch_array($rs1);
			return ucfirst($row1["username"]);
		}
	}
}
function addlog($txt){
$myfile = fopen("balanceupdatelog.txt", "a") or die("Unable to open file!");
fwrite($myfile, $txt);
fclose($myfile);
}
ini_set('memory_limit', '1024M');
set_time_limit(999999999);
?>
