<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<header class="header"> <a href="dashboard.php" class="logo">
  <!-- Add the class icon to your logo image or logo icon to add the margining -->
  &#2384; </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
    <div class="navbar-right">
      <ul class="nav navbar-nav">
        <li class="dropdown user user-menu"> 
          <a href="party.php" class="dropdown-toggle">Party</a>
        </li>
        <li class="dropdown user user-menu"> 
          <a href="item.php" class="dropdown-toggle">Item</a>
        </li>
        <li class="dropdown user user-menu"> 
          <a href="trade.php" class="dropdown-toggle">Trade</a>
        </li>
        <li class="dropdown user user-menu"> 
          <a href="billqty_report.php" class="dropdown-toggle">Bill Report</a>
        </li>
        <li class="dropdown user user-menu"> 
          <a href="parity_report.php" class="dropdown-toggle">Parity Report</a>
        </li>
        <li class="dropdown user user-menu"> 
          <a href="trademeta.php" class="dropdown-toggle">Test Report</a>
        </li>
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-user"></i> <span>admin<i class="caret"></i></span> </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header bg-light-blue"> <img src="<?php echo $baseUrl.'assets/';?>img/avatar3.png" class="img-circle" alt="User Image" />
              <p> Hello, Manojbhai <small>Admin Section</small> </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left"> <a href="#" class="btn btn-default btn-flat">Change Password</a> </div>
              <div class="pull-right"> <a href="index.php" class="btn btn-default btn-flat">Log out</a> </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
