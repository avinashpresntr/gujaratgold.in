<div id="mainDiv" class="wrapper row-offcanvas row-offcanvas-left">
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image"> <img src="<?php echo $baseUrl.'assets/';?>img/avatar3.png" class="img-circle" alt="User Image" /> </div>
      <div class="pull-left info">
        <p>admin</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a> </div>
    </div>
    <ul class="sidebar-menu">
      <li class="active"> <a href="dashboard.php"> <i class="fa fa-dashboard"></i> <span>Dashboard</span> </a> </li>
	  <li> <a href="party.php"><i class="fa fa-table"></i> <span>Party</span></a></li>
      <li> <a href="item.php"><i class="fa fa-table"></i> <span>Item</span></a></li>
      <li> <a href="testResult.php"><i class="fa fa-table"></i> <span>testResult</span></a></li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
