<?php
	include_once('includes/basepath.php');
	if(!isset($_SESSION['user_id'])){
		header('Location: ./index.php');
		exit;
	}

	require_once 'Mobile_Detect.php';
	$detect = new Mobile_Detect;
 
	// Any mobile device (phones or tablets).
	if ( $detect->isMobile() ) {
	$is_mobile = true;
?>
		<script>
			var is_mobile = true;
		</script>
<?php
	}else{
		$is_mobile = false;
?>
		<script>
			var is_mobile = false;
		</script>
<?php
	}
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" /> 
  <script type="text/javascript" src="js/jquery-ui.js"></script>  
</head>
<body>
		<div class="mainwrapper" style="margin:0px auto; padding:10px; text-align:center">
		 <form method="post" action="luckydraw.php" id="showtimeform">
			Date: <input type="text" id="frmdate" name="frmdate" placeholder="Select Date" value="<?php if (isset($_POST['frmdate'])) { echo $_POST['frmdate']; } ?>">
			<input type="button" name="showtime" value="Load Draw" id="printpurchase">
			<input type="button" name="back" value="Back" id="back">
		</div>
		<script language="javascript">
        $(document).ready(function() {
            $('#frmdate').datepicker({ dateFormat: 'dd-mm-yy' });
            
            $('#printpurchase').on('click', function() {
		            if ($('#frmdate').val() != '') {
		                $('#showtimeform').submit();
		            }
		            else {
		                alert("Please select date");
		                return false;
		            }
		        });
		        $('#back').on('click', function() {
					if(is_mobile == true){
						window.location.replace('dashboard_mobile.php');
					} else {
						window.location.replace('dashboard.php');
					}
		        });
		       
        })
        
    </script>
	<?php
		if(isset($_POST['frmdate'])){
		$frmdate = date('Y-m-d', strtotime($_POST['frmdate']));
		$sSQL = "SELECT win_product_id,DATE_FORMAT(drawdatetime,'%h:%i %p') as currentTime,is_jackpot,win_product_id2,is_jackpot2 FROM draw WHERE DATE_FORMAT(drawdatetime,'%Y-%m-%d') = '".$frmdate."' AND DATE_FORMAT(drawdatetime,'%Y-%m-%d') != '2014-11-04'";
		if($frmdate == date('Y-m-d')) $sSQL .= " AND DATE_FORMAT(drawdatetime,'%H:%i:%s') <= '".date("H:i:s")."'";
		$sSQL .= " ORDER BY drawdatetime";
		$rs = mysql_query($sSQL);
			
		if(mysql_num_rows($rs) > 0){
		?>
		<div id="yantra_content" style="border:1px solid black;margin:0px auto;padding:10px;background:none;border:0px;box-shadow:none">		
			<div style="font-size:20px; text-align:center; border-bottom:dotted 2px #514F4F; padding-bottom:5px; margin-bottom:5px;">Lucky Draw</div>
			<div style="text-align:center;margin-bottom:10px;"><b><span>From Date: <?php echo date("d/m/Y", strtotime($_POST['frmdate']));?></span></b></div>
			<div id="content" style="height:auto;">
			<?php if($package == 3): ?>
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<?php
					$rCount = 0;
					$list = "";
					while($row = mysql_fetch_array($rs)){
						
						$imgNo = $row["win_product_id"];
						if($imgNo > 0){
							if($imgNo < 10) $newImgNo = "0".$imgNo; else $newImgNo = $imgNo;
						}else{
							$newImgNo = "";
						}

						$imgNo2 = $row["win_product_id2"];
						if($imgNo2 > 0){
							if($imgNo2 < 10) $newImgNo2 = "0".$imgNo2; else $newImgNo2 = $imgNo2;
						}else{
							$newImgNo2 = "";
						}
						
						if(($rCount % 6 ) == 0){
							echo '</tr><tr>';
							$rCount = 0;
						}
						
						if($row["is_jackpot"] == 'YES' || $row["is_jackpot2"] == 'YES') $ijp = ' class="yello"'; else $ijp = '';
						if($row["is_jackpot"] == 'YES') $ljp = ' yelloborder'; else $ljp = '';
						if($row["is_jackpot2"] == 'YES') $rjp = ' yelloborder'; else $rjp = '';
												
						if($newImgNo != "" && $newImgNo2 != "" && $allow_twowin) $tcm = ' tow_colls'; else $tcm = '';
						echo '<td'.$ijp.' width="16.66%" height="119">';
							echo '<div class="yantralist'.$tcm.'">';
	              echo '<div class="lefttext">';
	                if($tcm != ""){
		                echo '<table width="100%" cellspacing="0" cellpadding="0" border="0" id="noborder">';
		        					echo '<tr>';
		        						echo '<td width="25%" style="border:0px;" class="'.$ljp.'"></td>';
		        						echo '<td width="50%" align="center" style="border:0px;">'.$row["currentTime"].'</td>';
		        						echo '<td width="25%" style="border:0px;" class="'.$rjp.'"></td>';
		        					echo '</tr>';
		        				echo '</table>';
	        				}else{
		                echo '<div class="lefttime">'.$row["currentTime"].'</div>';
		                echo '<div class="leftstart"></div>';
	              	}
	              echo '</div>';
	              if($tcm != ""){
	              	echo '<div class="rightyantra'.$ljp.' leftline">'.(($newImgNo!="")?'<img width="100" height="102" src="./images/'.$imageLoad.'GW'.$newImgNo.'.jpg" />':'').'</div>';
	              	echo '<div class="rightyantra'.$rjp.'">'.(($newImgNo2!="")?'<img width="100" height="102" src="./images/'.$imageLoad.'GW'.$newImgNo2.'.jpg" />':'').'</div>';
	              }else{
	              	echo '<div class="rightyantra">'.(($newImgNo!="")?'<img width="100" height="102" src="./images/'.$imageLoad.'GW'.$newImgNo.'.jpg" />':'').'</div>';
	              }
	            echo '</div>';
						echo '</td>';
						$rCount++;
					}
					if($rCount < 6){
	      		for($j=$rCount;$j<6;$j++){
	      			echo '<td></td>';
	      		}
	      		echo '</tr>';
	      	}
	      	//echo $list;
					?>
				</table>
			<?php else: ?>
				<ul>
					<?php 
					while($row = mysql_fetch_array($rs)){
						if($row["win_product_id"] < 10 && $row["win_product_id"] > 0) $winproductit = "0".$row["win_product_id"]; else $winproductit = $row["win_product_id"];
						if($row["win_product_id2"] < 10 && $row["win_product_id2"] > 0) $winproductit2 = "0".$row["win_product_id2"]; else $winproductit2 = $row["win_product_id2"];
						
						if($allow_twowin){
							if($winproductit != "" && $winproductit2 != "") $rightyantra = "rightyantra2"; else $rightyantra = "rightyantra";						
							if($row["is_jackpot"] == 'YES' || $row["is_jackpot2"] == 'YES'){
							?>
								<li><div class="yantralist jp"><div class="lefttext"><div class="lefttime"><?php echo $row["currentTime"]; ?></div><div class="leftstart"><img src="./images/star.gif" style="width:30px !important" height="25px" /></div></div><div class="<?php echo $rightyantra?> jackport"><?php if($winproductit != ''){ ?><img src="./images/<?php echo $imageLoad?>GW<?php echo $winproductit;?>.jpg" /><?php } ?><?php if($winproductit2 != ''){ ?><img src="./images/<?php echo $imageLoad?>GW<?php echo $winproductit2;?>.jpg" /><?php } ?></div></div></li>
							<?php 
							}else{
							?>
								<li><div class="yantralist"><div class="lefttext"><div class="lefttime"><?php echo $row["currentTime"]; ?></div><div class="leftstart"></div></div><div class="<?php echo $rightyantra?>"><?php if($winproductit != ''){ ?><img src="./images/<?php echo $imageLoad?>GW<?php echo $winproductit;?>.jpg" /><?php } ?><?php if($winproductit2 != ''){ ?><img src="./images/<?php echo $imageLoad?>GW<?php echo $winproductit2;?>.jpg" /><?php } ?></div></div></li>
							<?php
							}
							
						}else{					
							if($row["is_jackpot"] == 'YES'){
							?>
								<li><div class="yantralist jp"><div class="lefttext"><div class="lefttime"><?php echo $row["currentTime"]; ?></div><div class="leftstart"><img src="./images/star.gif" style="width:30px !important" height="25px" /></div></div><div class="rightyantra jackport"><img src="./images/<?php echo $imageLoad?>GW<?php echo $winproductit;?>.jpg" /></div></div></li>
							<?php 
							}else{
							?>
								<li><div class="yantralist"><div class="lefttext"><div class="lefttime"><?php echo $row["currentTime"]; ?></div><div class="leftstart"></div></div><div class="rightyantra"><img src="./images/<?php echo $imageLoad?>GW<?php echo $winproductit;?>.jpg" /></div></div></li>
							<?php
							}
						}
					} ?>
				</ul>
			<?php endif; ?>
			</div>
		</div>
		
		<?php
		}else{
			echo "No records found.";
		}
	}
	?>
</body>
</html>
