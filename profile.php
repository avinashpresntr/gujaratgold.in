<?php
include_once('includes/basepath.php');
if(!isset($_SESSION['user_id'])){
    header('Location: ./index.php');
    exit;
}
if(isset($_POST['change_password'])){
    $old_password = md5($_POST['old_password']);
    $user_id = $_SESSION['user_id'];
    $sql = "SELECT user_id FROM `users` WHERE password = '$old_password' AND user_id = '$user_id' LIMIT 1";
    $result = mysql_query($sql);
    if(mysql_num_rows($result) == 1){
        $new_password = md5($_POST['new_password']);
        mysql_query("UPDATE users SET password='$new_password' WHERE user_id = '$user_id'");
        $_SESSION['msg'] = "Password changed successfully!";
        header('Location: ./profile.php');
        exit;
    }else{
        $_SESSION['msg'] = "Old password not matched!";
        header('Location: ./profile.php');
        exit;
    }
}
?>
<html>
<head>
    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <script src="js/jquery-ui.js"></script>

    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <script language="javascript">
        jQuery(document).ready(function() {
            jQuery(document).on('submit','#frmChangePassword',function(e){
                var new_password = jQuery('#new_password').val();
                var confirm_password = jQuery('#confirm_password').val();
                if(new_password != confirm_password){
                    alert('New password and Confirm password not match!');
                    return false;
                }else{
                    return true;
                }
            });
        })
    </script>
</head>
<body>
<div class="top">
    <?php include_once('menu.php'); ?>
    <b><span style="float:right;">Hello,<?php if (isset($_SESSION['username'])) echo $_SESSION['username']; ?></span></b>
    <div class="clearfix"></div><br/>
    <span style="float:right;"><a href="logout.php">Logout</a></span>
    <div class="clearfix"></div>
</div>
<form id="frmChangePassword" method="POST" style="padding:20px;text-align:center;">
    <table align="center">
        <?php if (isset($_SESSION['msg'])) { ?>
            <div><?php echo $_SESSION['msg']; ?></div>
            <br/><br/>
            <?php
            unset($_SESSION['msg']);
        } ?>
        <tr>
            <td>Old Password: </td>
            <td><input type="text" id="old_password" name="old_password" placeholder="Old Password" required="required"></td>
        </tr>
        <tr>
            <td>New Password: </td>
            <td><input type="text" id="new_password" name="new_password" placeholder="New Password" required="required"></td>
        </tr>
        <tr>
            <td>Confirm Password: </td>
            <td><input type="text" id="confirm_password" name="confirm_password" placeholder="Confirm Password" required="required"></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input name="change_password" type="submit" value="Submit">
            </td>
        </tr>
    </table>
</form>
</body>
</html>
