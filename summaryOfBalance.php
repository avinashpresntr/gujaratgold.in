<?php include_once('includes/basepath.php'); 
if(!isset($_SESSION['username']) && $_SESSION['usertype'] == 0){
	header('Location: index.php');
}

if(isset($_REQUEST["act"]) && $_REQUEST["act"] != "" && $_REQUEST["user_id"] != ""){
	if($_REQUEST["act"] == 'delete' && $allowAdminDelete)
		mysql_query("DELETE FROM users WHERE user_id = ".$_REQUEST["user_id"]);
	if($allowAdminActiveDeactive){
		if($_REQUEST["act"] == 'deactive')
			mysql_query("UPDATE users SET is_active = 0 WHERE user_id = ".$_REQUEST["user_id"]);
		else
			mysql_query("UPDATE users SET is_active = 1 WHERE user_id = ".$_REQUEST["user_id"]);
	}
	header('Location: summaryOfBalance.php');
	exit;
}
unset($_SESSION['msg']);
?>

<html>
    <head> 
        <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" href="css/jquery-ui.css" /> 
        <script src="js/jquery-ui.js"></script>  

        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <script language="javascript">
            jQuery(document).ready(function() {
                jQuery('#date').datepicker({ dateFormat: 'dd-mm-yy' });
            })
        </script>
    </head>
    <body>
        <div class="top">
            <?php include_once('menu.php'); ?>
            <b><span style="float:right;">Hello,<?php if (isset($_SESSION['username'])) echo $_SESSION['username']; ?></span></b>
            <div class="clearfix"></div><br/>
            <span style="float:right;"><a href="logout.php">Logout</a></span>
            <div class="clearfix"></div>		
        </div>	
        <div style="margin-left:140px">
        <form method="POST" id="frmdata">
        	<b>Balance Summary - Filter</b> <select name="filter" onchange="frmdata.submit();">
        		<option value="username" <?php if(isset($_POST["filter"]) && $_POST["filter"] == "username") echo 'selected'; ?>>Username</option>
        		<option value="city,username" <?php if(isset($_POST["filter"]) && $_POST["filter"] == "city,username") echo 'selected'; ?>>City</option>
        		</select>
        </form>
      	</div>
            <?php
			  if(isset($_GET['pass']) && $_GET['pass'] == "Ok")
			    echo "Change password successfull";
			?>
        <table id="example1" border="1" cellspacing="0" cellpadding="2" class="table table-bordered table-striped" align="center" width="80%">
            <tr>
                <th>User Type</th>
                <th>Name of users</th>
                <th>City</th>
                <th>Current balance</th>
                <th>Commission</th>
                <th>Online</th>
                <th>Action</th>
            </tr>
            <?php
				$usertype = $_SESSION['usertype'];
            	if(isset($_POST["filter"]))
            	{
					if($usertype == 2)
					{
						$sql = "SELECT * FROM `users` where parent_id = ".$_SESSION['user_id']." ORDER BY ".$_POST["filter"];
					}
					else
					{
						$sql = "SELECT * FROM `users` ORDER BY ".$_POST["filter"];
					}
				}
                else
                {
					if($usertype == 2)
					{
						$sql = "SELECT * FROM `users` where parent_id = ".$_SESSION['user_id']." ORDER BY username";
					}
					else
					{
						$sql = "SELECT * FROM `users` ORDER BY username";
					}
				}
                $result = mysql_query($sql);
                while($row = mysql_fetch_array($result)) {
					if($row['usertype'] == 0){
						$user_type = 'Admin';
					}elseif($row['usertype'] == 1){
						$user_type = 'Retailer';
					}else{
						$user_type = 'Agent';
					}
                    echo "<tr>
                    <td>".$user_type."</td>
                    <td>".$row['username']."</td>
                    <td>".$row['city']."</td>
                    <td align='right'>".number_format($row['current_balance'],2,".","")."</td>
                    <td align='right'>".$row['user_commission']." </td>";
                    $datetime1 = strtotime($row['online']);
										$datetime2 = strtotime(date("Y-m-d H:i:s"));
										$interval  = abs($datetime2 - $datetime1);
										$minutes   = round($interval / 60);
                    if($minutes >= 15)
                    echo "<td align='center'>No</td>";
                    else
                    echo "<td align='center'>Yes</td>";
                    echo "<td align='center'>";
	                    if($allowAdminPassword) echo "<a href=changepass.php?user_id=".$row['user_id'].">Change Pass</a>&nbsp;<b>|</b>&nbsp;";
	                    if($allowAdminActiveDeactive):
		                    if($row["is_active"] == 1):
		                    echo "<a href='summaryOfBalance.php?act=deactive&user_id=".$row['user_id']."'>Deactive</a>&nbsp;<b>|</b>&nbsp;";
		                    else:
		                    echo "<a href='summaryOfBalance.php?act=active&user_id=".$row['user_id']."'>Active</a>&nbsp;<b>|</b>&nbsp;";
		                    endif;
		                  endif;
                      if($allowAdminEdit) echo "<a href='addretailer.php?act=1&id=".$row['user_id']."'>Edit</a>&nbsp;<b>|</b>&nbsp;";
                      if($allowAdminDelete) echo "<a href='javascript:void(0)' onclick='confirmtoDelete(".$row['user_id'].")'>Delete</a>";
                    echo "</td>";
                    echo "</tr>";
                }
            ?>
        </table>
    </body>
</html>
<script language="javascript">
	function confirmtoDelete(cid){
		var r = confirm("Confirm to delete this retailer?");
		if (r == true) {
		    location.replace("summaryOfBalance.php?act=delete&user_id="+cid);
		}
	}
	</script>
<?php
// if (isset($_POST['submit']) && $_POST['username'] != '' && $_POST['password'] != '') {
//     $username = mysql_real_escape_string($_POST['username']);
//     $pass = md5($_POST['password']);
//     //$bal = $_POST['balance'];
//     $utype = $_POST['usertype'];
    
//     $sql = "INSERT INTO users SET "
//         ."username = '".$username."',"
//         ."password = '".$pass."',"
//         //."current_balance = '".$bal."',"
//         ."usertype = '".$utype."'"; //VALUES('$username','$pass','$bal','$utype')";
//     mysql_query($sql);
    
//     $_SESSION['msg'] = 'Retailer Added Successfully';
    
// }
?>
