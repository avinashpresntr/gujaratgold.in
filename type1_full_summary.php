<?php
if(isset($_POST["submitBtn"]) && $_POST["cmbSummary"]=='full')
{
	$postUserId = $_POST["user_id"];
	if($postUserId=="")
	{
		$qrySel = "SELECT SUM( quantity * product_price ) as totalBought , DATE( receipt_master.receipt_time ) as receiptDate, receipt_master.retailer_id, SUM( (quantity * product_price) * user_commission /100 ) AS totalCommission FROM receipt_master LEFT JOIN receipt_details ON receipt_master.receipt_id = receipt_details.receipt_id WHERE DATE( receipt_master.receipt_time ) BETWEEN '".$_POST['from_date']."' AND '".$_POST['to_date']."' GROUP BY receipt_master.retailer_id ASC";
	}
	else
	{
		$qrySel = "SELECT SUM( quantity * product_price ) as totalBought , DATE( receipt_master.receipt_time ) as receiptDate, receipt_master.retailer_id, SUM( (quantity * product_price) * user_commission /100 ) AS totalCommission FROM receipt_master LEFT JOIN receipt_details ON receipt_master.receipt_id = receipt_details.receipt_id WHERE receipt_master.retailer_id='".$postUserId."' AND DATE( receipt_master.receipt_time ) BETWEEN '".$_POST['from_date']."' AND '".$_POST['to_date']."' GROUP BY  receipt_master.retailer_id ASC";
	}	
	$resSel = mysql_query($qrySel);
	if(mysql_num_rows($resSel)>0)
	{
		?>
		<table id="example1" cellpadding="4" cellspacing="0" border="1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>User</th>
						<th>Opening</th>
						<th>Credit</th>
						<th>Debit</th>
						<th>Bought</th>
						<th>Comm.</th>
						<th>Win</th>
						<th>Balance</th>
					</tr>
				</thead>
				<tbody>
		<?php
		while($qFetch = mysql_fetch_array($resSel))
		{
			$retailerId = $qFetch["retailer_id"];
			$receiptDate = $qFetch["receiptDate"];
			$totalCommision = $qFetch["totalCommission"];
			
			$qrySelUser = "SELECT user_id,username FROM users WHERE user_id='".$retailerId."'";
			$resSelUser = mysql_query($qrySelUser);
			$qFetchUser = mysql_fetch_array($resSelUser);
			
			$userName = $qFetchUser["username"];
			
			$qrySelTrans = "SELECT * FROM transaction WHERE retailer_id='".$retailerId."' AND DATE(transaction_time) BETWEEN '".$_POST['from_date']."' AND '".$_POST['to_date']."'";
			$resSelTrans = mysql_query($qrySelTrans);
			$creditAmount = $debitAmount = 0;
			if(mysql_num_rows($resSelTrans)>0)
			{
				while($qFetchTrans = mysql_fetch_array($resSelTrans))
				{
					if($qFetchTrans["cr_dr"]=='Credit')
					{
						$creditAmount += $qFetchTrans['amount'];
					}
					if($qFetchTrans["cr_dr"]=='Debit')
					{
						$debitAmount += $qFetchTrans['amount'];
					}
				}
			}
			$totalBought = $qFetch["totalBought"];
			
			//----- code for opening balance ----
			$previousDate = date('Y-m-d', strtotime($_POST['from_date'] .' -1 day'));
			$qrySelOpeningBalance = "SELECT SUM( quantity * product_price ) as totalBought , 
									DATE( receipt_master.receipt_time ) as receiptDate, receipt_master.retailer_id, SUM( (quantity * product_price) * user_commission /100 ) AS totalCommission FROM receipt_master LEFT JOIN receipt_details ON receipt_master.receipt_id = receipt_details.receipt_id WHERE receipt_master.retailer_id='".$retailerId."' AND DATE( receipt_master.receipt_time ) <= '".$previousDate."'
									GROUP BY receipt_master.retailer_id ASC";
			$resSelOpeningBalance = mysql_query($qrySelOpeningBalance);
			$numRowsOpeningBalance = mysql_num_rows($resSelOpeningBalance);
			$openingBalance = 0;
			if($numRowsOpeningBalance>0)
			{
				$qrySelTrans1 = "SELECT * FROM transaction WHERE retailer_id='".$retailerId."' AND DATE(transaction_time) <= '".$previousDate."'";
				$resSelTrans1 = mysql_query($qrySelTrans1);
				$creditAmount1 = $debitAmount1 = 0;
				if(mysql_num_rows($resSelTrans1)>0)
				{
					while($qFetchTrans1 = mysql_fetch_array($resSelTrans1))
					{
						if($qFetchTrans1["cr_dr"]=='Credit')
						{
							$creditAmount1 += $qFetchTrans1['amount'];
						}
						if($qFetchTrans1["cr_dr"]=='Debit')
						{
							$debitAmount1 += $qFetchTrans1['amount'];
						}
					}
				}
				$totalCommision1 = $qFetch["totalCommission"];
				$totalBought1 = $qFetch["totalBought"];
				$win1 = 0;
				$openingBalance = $creditAmount1 - $debitAmount1 - $totalBought1 + $totalCommision1 + $win1;
			}
			//------------------------------------
			
			//= o+c-d-b+c+w=b
			$opening = 0;
			$win = 0;
			$totalBalance = $openingBalance + $creditAmount - $debitAmount - $totalBought + $totalCommision + $win;
			?>
			<tr>
				<td><?php echo $userName; ?></td>
				<td><?php echo $openingBalance; ?></td>
				<td align="right"><?php echo $creditAmount; ?></td>
				<td align="right"><?php echo $debitAmount; ?></td>
				<td><?php echo $totalBought; ?></td>
				<td><?php echo $totalCommision; ?></td>
				<td>0</td>
				<td><?php echo $totalBalance; ?></td>
			</tr>
				
			<?php
		}
		?>
		</tbody>
				<!-- <tfoot>
					<tr>                                                
						<th colspan="2" align="right">Total</th>
						<th colspan="3"></th>
					</tr>
				</tfoot> -->
			</table>
		<?php
	}
}
?>
	